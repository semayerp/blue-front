import { useState, Fragment, useEffect} from "react"
import { useStyle } from "../../store/theme";
import { AppSpinner } from "../spinner";
import { useLogInStore } from "../../store/store";
import { RefreshIcon, TrashIcon, TrayDownIcon, TrayUpIcon} from "../../components/icons";
import ErrorBoundary from "../../components/errorboundary"
import { useGetSingleEmployeeHook } from "../../appfetch/hrm/employee";
import { useParams } from "react-router-dom";
import { TabNormalButton, NormalButton} from "../../components/button";
import { SelectInput } from "../../components/input";
import moment from "moment";



export function EditableGetEmployeeComponent( {item, index} ){
    const [tabToken,setTabToken]=useState(1)   

    return (
    <Fragment key={'form-row'+index}>
        <div  className="w-full  text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
            <div className="flex bg-gray-50 w-1/12 justify-center items-center">
                <p>{item.id}</p>
            </div>
            <div className="flex justify-center items-center break-word bg-gray-50 w-3/12 text-wrap">
                <p className="p-3">{item.first_name} {item.middle_name} {item.last_name}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-2/12 text-ellipsis">
                <p> {moment(item.birth_date).format("MMM Do YY")}</p>
            </div>
            <div className="flex justify-center capitalize items-center bg-gray-50 w-2/12 ">
                <p> {moment(item.hire_date).format("MMM Do YY")}</p>
            </div>
            
            <div className="flex justify-center items-center w-4/12 break-all bg-gray-50 ">
                <p className="p-3" > {item.gross_salary}</p> 
            </div>
        </div>
        <br/>
        <div className="w-full flex items-stretch justify-start h-full ">
            <div className="w-full tabs">
                <div className="tab-list flex flex-row p-1 bg-orange-50 flex-wrap space-x-1">
                    <div className="tab" onClick={()=>setTabToken(1)}>
                        <TabNormalButton index={1}  token={tabToken} label="Use Cases" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(2)}>
                        <TabNormalButton index={2} token={tabToken} label="Address" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(3)} >
                        <TabNormalButton index={3} token={tabToken} label="Documents" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(4)}>
                        <TabNormalButton index={4} token={tabToken} label="Leave" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(5)}>
                        <TabNormalButton index={5} token={tabToken} label="Job Description" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(6)}>
                        <TabNormalButton index={6} token={tabToken} label="Banks" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(7)}>
                        <TabNormalButton index={7} token={tabToken} label="Email" />
                    </div>
                </div>
                <div className={tabToken == 1 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                    <div className="flex w-full content-center items-center justify-center h-full  p-0">
                    <div className="flex flex-row flex-wrap m-1 items-stretch justify-start min-w-0 break-words w-full border-0 p-2">    
                        <div className="max-w-sm rounded m-1 overflow-hidden  bg-orange-50 shadow-lg">   
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Adding Roles To System User</div>
                                <p className="text-gray-700 text-base">
                                The Added Roles to Users refers to attaching privlege to a user to provided access to endpoints 
                                that require the specified roles
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#adduserrole</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#access</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#systemdesign</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                        
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Resetting User Password </div>
                                <p className="text-gray-700 text-base">
                                Resets Password to "default@123" and forces user to Change password when logging in.

                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                        
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Disable User </div>
                                <p className="text-gray-700 text-base">
                                If user Disabled value is "True"; it means user will not be allowed to login.
                                
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div> 
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Add Pages To Users </div>
                                <p className="text-gray-700 text-base">
                                As long as the attached Page is active;User will have Access to Operations.
                                In addtion the specified page will not be added to 
                                the user if User does not have at least one role that is required by the Page
                                for access.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Remove Pages From Users </div>
                                <p className="text-gray-700 text-base">
                                Revokes access to the specified page to User if removed.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>  
                    </div>
                    </div>
                
                </div>
                <div className={tabToken == 2 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                   
                </div>
                <div className={tabToken == 3 ? "tab-panel w-full flex flex-row flex-wrap items-stretch justify-center pt-5 pb-8" : "hidden"} >
                             
                </div>    
                <div className={tabToken == 4 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                  
                </div>
                <div className={tabToken == 5 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                  
                </div>
                <div className={tabToken == 6 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                  
                </div>
                <div className={tabToken == 7 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                  
                </div>
            </div> 
        </div>
        
    </Fragment>
    )
}

function GetEmployeeComponent({renderData}){
    return (
        
        <>
        
            <EditableGetEmployeeComponent  item={renderData.data} index={1} />
                    
        </>
    )
}

export function EditableSingleEmployeeComponentMobile( {item, index} ){
    const [tabToken,setTabToken]=useState(1) 
    const [view,setView]=useState(false)
    return (
        <Fragment key={'mobile-form-row'+index}>                   
            {/* # */}
            <div key={index+'-mobile'} className="w-full bg-slate-50 shadow-xl rounded-xl p-2 flex space-y-1 flex-col items-stretch justify-center" >
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 bg-gray-200 p-1">ID </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300">{item.id} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 break-words p-1 bg-gray-200">Full Name</div>
                    <div className="flex justify-center items-center w-8/12 p-1 break-all bg-gray-300">{item.first_name} {item.middle_name} {item.last_name} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 break-words p-1 bg-gray-200 ">Birth Date</div>
                    <div className="flex justify-center items-center w-8/12 break-all p-1 bg-gray-300 indent-3">{moment(item.birth_date.toString()).format("MMM Do YY")}</div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 break-words bg-gray-200 ">Hire Date </div>
                    <div className="flex justify-center items-center w-8/12 p-1 capitalize bg-gray-300 indent-3">{moment(item.hire_date.toString()).format("MMM Do YY")}</div>
                </div> 
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">Salary </div>
                    <div className="flex justify-center items-center w-8/12 p-1 break-all bg-gray-300 indent-3">{item.gross_salary}</div>
                </div> 
                <div className="w-full flex flex-row h-10 justify-center" >
                    <button onClick={()=>setView(!view)}>
                    { view ? <TrayUpIcon /> : <TrayDownIcon /> }
                    </button>
                </div> 
            
                <div className={ view ? "w-full flex flex-row justify-center" : "hidden" }>
                <div className="w-full tabs">
                <div className="tab-list flex flex-row bg-orange-50 p-1 flex-wrap space-x-1">
                    <div className="tab" onClick={()=>setTabToken(1)}>
                        <TabNormalButton index={1}  token={tabToken} label="Use Cases" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(2)}>
                        <TabNormalButton index={2} token={tabToken} label="Address" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(3)} >
                        <TabNormalButton index={3} token={tabToken} label="Documents" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(4)}>
                        <TabNormalButton index={4} token={tabToken} label="Leave" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(5)}>
                        <TabNormalButton index={5} token={tabToken} label="Job Description" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(6)}>
                        <TabNormalButton index={6} token={tabToken} label="Banks" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(7)}>
                        <TabNormalButton index={7} token={tabToken} label="Email" />
                    </div>
                </div>
                <div className={tabToken == 1 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                <div className="flex w-full content-center items-center justify-center h-full  p-0">
                    <div className="flex flex-row flex-wrap m-1 items-stretch justify-start min-w-0 break-words w-full border-0 p-2">    
                        <div className="max-w-sm rounded m-1 overflow-hidden  bg-orange-50 shadow-lg">   
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Adding Roles To System User</div>
                                <p className="text-gray-700 text-base">
                                The Added Roles to Users refers to attaching privlege to a user to provided access to endpoints 
                                that require the specified roles
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#adduserrole</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#access</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#systemdesign</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                        
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Resetting User Password </div>
                                <p className="text-gray-700 text-base">
                                Resets Password to "default@123" and forces user to Change password when logging in.

                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                        
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Disable User </div>
                                <p className="text-gray-700 text-base">
                                If user Disabled value is "True"; it means user will not be allowed to login.
                                
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div> 
                    </div>
                    </div>
                
                </div>
                <div className={tabToken == 2 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                    
                </div>
                <div className={tabToken == 3 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                               
                </div>    
                <div className={tabToken == 4 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                 
                </div>
                <div className={tabToken == 5 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                 
                </div>
                <div className={tabToken == 6 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                 
                </div>
                <div className={tabToken == 7 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                 
                </div>
                </div> 
                </div>                             
            </div>
                          
        </Fragment>
    )
}

export function GetSingleComponentMobile({renderData}){
    return(
    <>
        <EditableSingleEmployeeComponentMobile item={renderData.data} index={1} />
    </>
    )
}

export function SingleUsersSection( { id }){
    
    const access_token = useLogInStore((state)=>state.access_token)
    const {isLoading, isSuccess, refetch,data } =useGetSingleEmployeeHook(id,access_token)
     if (isLoading){
        return <AppSpinner />
     }
     if (isSuccess){
        let renderData = data;        
    return(
        <Fragment>
             
            <div className="w-full flex items-stretch  justify-start h-auto  ">
                <div className="flex flex-col space-y-2  items-center justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pt-5 ">
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto px-4 lg:px-8 py-8 pt-0">
                        <div className="w-full h-full flex flex-row items-start justify-end">
                            
                            <div className="flex justify-center items-center w-8 h-8 rounded bg-slate-700">
                                <button onClick={refetch}>
                                    <RefreshIcon />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="pc-block hidden md:block w-full"> 
                    <div className="w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
                            <div className="flex bg-slate-700 w-1/12 rounded-tl-lg justify-center items-center">
                                <p>ID</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-3/12">
                                <p>Full Name</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-2/12">
                            <p> Birth Date</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-2/12">
                            <p>Hire Date</p>
                            </div>
                            <div className="flex justify-center items-center w-2/12  bg-slate-700">
                            <p>Salary</p> 
                            </div>
                            <div className="flex justify-center items-center w-2/12 rounded-tr-lg bg-slate-700">
                 
                            </div>
                    </div>
                    <ErrorBoundary>
                        <GetEmployeeComponent renderData={renderData} />
                    </ErrorBoundary> 
                    </div>
                    <div className="mobile-block md:hidden w-full flex-auto px-4 lg:px-10 py-10 pt-0">            
                        <div className="w-full flex flex-col items-stretch h-auto space-y-1"> 
                            <ErrorBoundary>
                                <GetSingleComponentMobile renderData={renderData} />
                            </ErrorBoundary>      
                        </div>
                    </div>
                
                </div>        
            </div>
        </Fragment>
         )      
     }      
}

export function SingleEmployeePage(){
    const myContainer=useStyle((state)=>state.styles.componentWorkingDiv)   
    const { id } = useParams()
    return(

        <div className={myContainer}>          
            <title>Employee Details</title>
            <div className="bg-zinc-100 shadow-lg h-auto pb-5 w-full ">
                <ErrorBoundary>
                    <SingleUsersSection id={id} />
                </ErrorBoundary>
            </div> 

        </div>
    )
    
}
 