import { useState, Fragment} from "react"
import { useStyle } from "../../store/theme";
import { AppSpinner } from "../spinner";
import { useLogInStore } from "../../store/store";
import { RefreshIcon} from "../../components/icons";
import ErrorBoundary from "../../components/errorboundary"
import { useGetUsersHook, usePatchActivateUserHook, usePatchDeactivateUserHook, usePatchResetPasswordHook } from "../../appfetch/useradmin/users";
import { Link } from "react-router-dom";
import { Pagination } from "../../components/pagination";

export function ActivateUserButton({ uid }){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate } = usePatchActivateUserHook()
    const activateUser = () => { 
        mutate({uid, access_token})
    }    
    return (
        <div className="flex w-full sm:w-8/12 justify-center items-center" >
            <button className="bg-gray-400 w-10/12 h-10 text-xl font-bold rounded-lg transition ease-in-out duration-75 hover:-translate-y-1 hover:scale-110 text-green-700" onClick={()=>activateUser()} >
                Enable
            </button>
        </div>
        )  

}  

export function DeactivateUserButton({ uid }){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate} = usePatchDeactivateUserHook()
    const deactivateUser = () => { 
        mutate({uid, access_token})
    }    
   
        return (
            <div className="flex w-full sm:w-8/12 justify-center items-center" >
                <button className="bg-gray-400 w-10/12 h-10 text-xl font-bold rounded-lg transition ease-in-out duration-75 hover:-translate-y-1 hover:scale-110 text-red-700" onClick={deactivateUser} >
                Disable 
                </button>
            </div>
            )  
    

}  

export function ResetPasswordButton({ uid }){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate} = usePatchResetPasswordHook()
    const resetPassword = () => { 
        mutate({uid, access_token})
    }    
   
        return (
            <div className="flex w-full sm:w-8/12 justify-center items-center" >
                <button className="bg-gray-400 w-10/12 h-10 text-xl font-bold rounded-lg transition ease-in-out duration-75 hover:-translate-y-1 hover:scale-110 text-orange-700" onClick={resetPassword} >
                Reset Password 
                </button>

            </div>
            )  
    

}  

export function EditableGetUserComponent( {item, index} ){
       
    return (
    <Fragment key={'form-row'+index}>
        <div  className="w-full  text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
            <div className="flex bg-gray-50 w-1/12 rounded-tl-lg justify-center items-center">
                <p>{item.id}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-3/12 text-wrap">
                <p className="p-3">{item.uid}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-3/12 text-ellipsis">
                <p> {item.email}</p>
            </div>
            <div className="flex justify-center capitalize items-center bg-gray-50 w-1/12 ">
                <p> {item.disabled.toString()}</p>
            </div>
            <div className="flex justify-center items-center w-4/12 break-all bg-gray-50 ">
                <p className="p-3" > {item.password}</p> 
            </div>
        </div>
        <div  className="w-full  text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
            <ErrorBoundary>
                <ResetPasswordButton uid={item.uid} />
            </ErrorBoundary>
            <ErrorBoundary>

                { item.disabled ?
                <ActivateUserButton  uid={item.uid}/>:
                <DeactivateUserButton uid={item.uid} />
                
                }
            </ErrorBoundary>
            <div className="flex justify-center items-center w-4/12">
            <Link  to={`/users/${item.id}`}className="bg-gray-400 w-10/12 h-10 py-2  text-xl font-bold rounded-lg transition ease-in-out duration-75 hover:-translate-y-1 hover:scale-110 text-black text-center" >
                Detials 
                </Link>
            </div>
        </div>
         
    </Fragment>
    )
}

function GetUsersComponent({renderData}){
    
    return (
        <>
        {
            renderData.map((x,index) =>{
                return(
                    <Fragment key={"editable"+index} >
                    <EditableGetUserComponent  item={x} index={index} />
                    </Fragment>
                )
            })
        }
        </>
    )
}

export function EditableGetUserComponentMobile( {item, index} ){
   
    return (
        <Fragment key={'mobile-form-row'+index}>                   
            {/* # */}
            <div key={index+'-mobile'} className="w-full bg-slate-50 shadow-xl rounded-xl p-2 flex space-y-1 flex-col items-stretch justify-center" >
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 bg-gray-200 p-1">ID </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300">{item.id} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200">UID </div>
                    <div className="flex justify-center items-center w-8/12 p-1 break-all bg-gray-300">{item.uid} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">email </div>
                    <div className="flex justify-center items-center w-8/12 break-all p-1 bg-gray-300 indent-3">{item.email}</div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1  bg-gray-200 ">Disabled </div>
                    <div className="flex justify-center items-center w-8/12 p-1 capitalize bg-gray-300 indent-3">{item.disabled.toString()}</div>
                </div> 
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">Password </div>
                    <div className="flex justify-center items-center w-8/12 p-1 break-all text-ellipsis bg-gray-300 indent-3">{item.password}</div>
                </div> 
                
                <div className="w-full flex flex-row justify-center">
                <Link  to={`/users/${item.id}`}className="bg-gray-400 w-10/12 h-10 py-2  text-xl font-bold rounded-lg transition ease-in-out duration-75 hover:-translate-y-1 hover:scale-110 text-black text-center" >
                Detials 
                </Link>
                </div>                              
            </div>
                          
        </Fragment>
    )
}

export function GetUsersComponentMobile({renderData}){
    return(
    <>
    {
        renderData.map((x,index) =>{       
            return (
                <Fragment key={'mobile-one'+index}>
                    <EditableGetUserComponentMobile item={x} index={index} />
                </Fragment>
            )    
        })
    }
    </>
    )
}

export function ViewUsersSection(){
    const [page, setPage]= useState(1)
    const [pageSize,setPageSize] = useState(3)
    const [sPage, setSpage]= useState(1)
    const access_token = useLogInStore((state)=>state.access_token)
    const [searchText,setSearchText] = useState('')
    
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setSearchText(value) 
    };
    
    const {data, isLoading, isSuccess, refetch} =useGetUsersHook(page,pageSize,access_token)
     if (isLoading){
        return <AppSpinner />
     }  
  
     if (isSuccess){
        let renderData;
        const sizeDropDown= Array.from({length : 50},(_,i) => i+1)
        if (searchText != ''){
            renderData=data?.data.items.filter(item => {
               return item.email.toLowerCase().includes(searchText.toLowerCase()) 
            })
        }else{
            renderData = data?.data.items
        }
         return(
        <Fragment>
            <div className="w-full flex items-stretch  justify-start h-full ">
                <div className="flex flex-col space-y-2  items-center justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pt-5 ">
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto px-4 lg:px-8 py-8 pt-0">
                        <div className="w-full h-full flex flex-row items-stretch justify-end">
                            <div className="flex justify-center items-center w-10/12 sm:w-3/12 ">
                                <input value={searchText}  onChange={handleInputChange} maxLength="50" className="rounded-lg w-9/12 text-black" placeholder="search text here .." name="search" type="search"/>
                            </div>
                            <div className="flex justify-center items-center w-10 h-full rounded bg-slate-700">
                                <button onClick={refetch}>
                                    <RefreshIcon />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-start h-auto px-4 lg:px-8 py-8 pt-0">
                    <Pagination 
                    page={page} 
                    pageSize={pageSize} 
                    setPage={setPage.bind(this)} 
                    setPageSize={setPageSize.bind(this)} 
                    maxPage={data.data.pages} 
                    sPage={sPage}
                    setSpage={setSpage.bind(this)}
                    options={sizeDropDown} />
                    </div>
                    <div className="pc-block hidden md:block w-full flex-auto space-y-1  px-4 lg:px-8 py-8 pt-0">
                    <div className="w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
                            <div className="flex bg-slate-700 w-1/12 rounded-tl-lg justify-center items-center">
                                <p>ID</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-3/12">
                                <p>UID</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-3/12">
                            <p> Email</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-1/12">
                            <p>Disabled</p>
                            </div>
                            <div className="flex justify-center items-center w-4/12 rounded-tr-lg bg-slate-700">
                            <p> Password</p> 
                            </div>
                    </div>
                    <ErrorBoundary>
                        <GetUsersComponent renderData={renderData} />
                    </ErrorBoundary>             
                    </div>

                    <div className="mobile-block md:hidden w-full flex-auto px-4 lg:px-10 py-10 pt-0">            
                        <div className="w-full flex flex-col items-stretch h-auto space-y-1"> 
                            <ErrorBoundary>
                                <GetUsersComponentMobile renderData={renderData} />
                            </ErrorBoundary>      
                        </div>
                    </div>
                
                </div>        
            </div>
        </Fragment>
         )      
     }     
}

export function UsersPage(){
    const myContainer=useStyle((state)=>state.styles.componentWorkingDiv)   
    return(

        <div className={myContainer}>          
            <title>Users</title>
            
            <div className="h-full bg-zinc-100 shadow-lg w-full">
                <ErrorBoundary>
                    <ViewUsersSection />
                </ErrorBoundary>
            </div> 

        </div>
    )
    
}
 