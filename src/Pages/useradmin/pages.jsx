import { useState, Fragment} from "react"
import { useStyle } from "../../store/theme";
import { AppSpinner } from "../spinner";
import { useLogInStore } from "../../store/store";
import { RefreshIcon, TrayDownIcon, TrayUpIcon} from "../../components/icons";
import ErrorBoundary from "../../components/errorboundary"
import { useGetRouteDropDownHook } from "../../appfetch/useradmin/roles";
import {  useAddPageRouteHook ,useAddPagesHook, useDeletePageHook, useDeletePageRouteHook, useGetPagessHook, usePatchActivatePageHook, usePatchDeactivatePageHook } from "../../appfetch/useradmin/pages";
import { SelectInput, SingleInput } from "../../components/input";
import { NormalButton, TabNormalButton } from "../../components/button";
import { TrashIcon } from "../../components/icons";
import { Pagination } from "../../components/pagination";


export function AddPagesForm(){
   
    const access_token = useLogInStore((state)=>state.access_token)    
    const [values, setValues] = useState({
        "name": "",
        "app":"",
        "description": "",
    });

    const { mutate } = useAddPagesHook()
    const handleClick =()=>{
        // console.log(values)
        mutate({values,access_token})  
    }
    
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setValues((values) => ({
           ...values,
           [target.name] : value,
        }));
       
    };


    return(
        <Fragment>
           <div className="flex w-full content-center items-center justify-center h-full ">
                <div className="flex flex-col items-center justify-center min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0">                    
                    <div className="w-full flex-auto px-4 lg:px-10 py-10 pt-0">
                        <form className="p-5 w-full">
                                <div className="flex flex-col md:flex-row space-x-2 w-full" >
                                <div></div>
                                <SingleInput 
                                name="name" 
                                label="Name"  
                                inputType="text" 
                                placeHolder="Role Name" 
                                value={values.name} 
                                handler={handleInputChange.bind(this)}  /> 
                                <SingleInput 
                                name="app" 
                                label="App"  
                                inputType="text" 
                                placeHolder="Admin" 
                                value={values.app} 
                                handler={handleInputChange.bind(this)} />                               
                                <SingleInput 
                                name="description" 
                                label="Description"  
                                inputType="text" 
                                placeHolder="HR Administrator" 
                                value={values.description} 
                                handler={handleInputChange.bind(this)} />
                                </div>

                                <div className="flex flex-col md:flex-row space-x-2 w-full" > 
                                <div></div> 
                                <div className="w-full flex flex-col items-end">
                                    <div className="w-full sm:w-3/12">
                                    <NormalButton 
                                    label="Add Page" 
                                    handleClick={handleClick} />     
                                    </div>
                                </div>  

                                </div>
                                
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
    )
    
}

export function ActivatePageButton({ page_id }){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate } = usePatchActivatePageHook()
    const activateUser = () => { 
        mutate({page_id, access_token})
    }    
    return (
        <div className="flex w-full sm:w-8/12 justify-center items-center" >
            <button className="bg-gray-300 w-10/12 h-10 text-xl font-bold rounded-lg transition ease-in-out duration-75 hover:-translate-y-1 hover:scale-110 text-green-500" onClick={()=>activateUser()} >
                Enable
            </button>
        </div>
        )  

}  

export function DeactivatePageButton({ page_id }){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate} = usePatchDeactivatePageHook()
    const deactivateUser = () => { 
        mutate({page_id, access_token})
    }    
   
        return (
            <div className="flex w-full sm:w-8/12 justify-center items-center" >
                <button className="bg-gray-300 w-10/12 h-10 text-xl font-bold rounded-lg transition ease-in-out duration-75 hover:-translate-y-1 hover:scale-110 text-red-500" onClick={()=>deactivateUser()} >
                Disable 
                </button>
            </div>
            )  
    

} 

function DeletePageButton({page_id}){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate } = useDeletePageHook()
    const deleteRoute = () => {
        
        mutate({page_id, access_token})
    }    
    
    return (
        <button onClick={()=>deleteRoute()} >
            <TrashIcon />
        </button>
        )
}


function DeletePageRouteButton({ page_id , route_id}){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate } = useDeletePageRouteHook()
    const deleteRoutePage = () => {
        
        mutate({page_id, route_id, access_token})
    }    
    
    return (
        <button onClick={()=>deleteRoutePage()} >
            <TrashIcon />
        </button>
        )
}

export function RoutesDropDown({name, label, page_id, value, handler }){
    const access_token = useLogInStore((state)=>state.access_token)
    const { data, isSuccess, refetch } = useGetRouteDropDownHook(access_token,page_id)
    if(isSuccess) {    
        return( <>
                <SelectInput data={data.data} name={name} value={value} handler={handler} label={label} />  
            
                </>
            )
        }
}

export function AddRouteToPageForm( {page_id} ){
    const access_token = useLogInStore((state)=>state.access_token)    
    const { mutate } = useAddPageRouteHook()
    const [value, setValue] = useState();
    const handleClick =()=>{
        
        if ( value != "select to add"){
            mutate({route_id:value,page_id,access_token})
            
        }
    }
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setValue(value);
      
    };
 

    return(
        <Fragment>
           <div className="flex w-full content-center items-center justify-center h-full ">
                <div className="flex flex-col items-center justify-center min-w-0 break-words w-full shadow-lg rounded-lg bg-sky-50 border-0">                    
                    <div className="w-full flex-auto px-4 lg:px-10 py-10 pt-0">
                        <form className="p-5 w-full">
                                <div className="flex flex-col md:flex-row space-x-2 w-full" >
                                <div></div>
                                <ErrorBoundary>
                                    <RoutesDropDown 
                                    name="route" 
                                    label="Routes"      
                                    value={value}                                   
                                    page_id={page_id}
                                    handler={handleInputChange.bind(this)}
                                    />
                                </ErrorBoundary>
                                </div>                                 
                                <div className="flex flex-col md:flex-row space-x-2 w-full" > 
                                <div></div> 
                                <div className="w-full flex flex-row justify-end items-end">
                                    
                                    <NormalButton 
                                    label="Add Route" 
                                    handleClick={handleClick} />                                       
                                </div>  

                                </div>
                                
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
    )
    
}

export function EditableGetPagesComponent( {item, index} ){
    const [tabToken,setTabToken]=useState(1) 
    const [view,setView]=useState(false)
    return (
    <Fragment key={'form-row'+index}>
        <div  className="w-full  text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
            <div className="flex bg-gray-50 w-1/12  justify-center items-center">
                <p>{item.id}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-2/12 text-wrap">
                <p className="p-3">{item.name}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-3/12 text-ellipsis p-3">
            <p> {item.app}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-2/12 text-wrap">
                <p className="p-3">{item.active.toString()}</p>
            </div>
            <div className="flex justify-center capitalize break-all items-center bg-gray-50 w-2/12 p-3 ">
                <p> {item.description}</p>
            </div>
            <div className="flex justify-center capitalize break-all items-center bg-gray-50 w-1/12 ">
                <ErrorBoundary>
                    <DeletePageButton page_id={item.id} />
                </ErrorBoundary>
            </div>
            <div className="flex justify-center capitalize break-all items-center bg-gray-50 w-1/12 ">
                <button onClick={()=>setView(!view)}>
                    { view ? <TrayUpIcon /> : <TrayDownIcon />}
                </button>
            </div>
        </div>
        <div  className={view ? "w-full bg-gray-50 text-lg flex flex-row flex-wrap items-stretch justify-start h-auto" : "hidden"}>
            <div></div>
            {
            item.routes.map((route,index)=>{
                return (
                    <div key={index+route.route_path} className="flex w-2/12 bg-slate-50 p-1 rounded-tl-lg flex-row justify-start items-stretch" >
                        <div className="w-9/12 bg-slate-400 flex items-center break-all p-1 justify-center">
                            <p>{route.route_path}</p>
                        </div>
                        <div className="w-3/12 flex items-center justify-center bg-slate-300">
                            <ErrorBoundary>
                                <DeletePageRouteButton route_id={route.id} page_id={item.id} />
                            </ErrorBoundary>
                        </div>
                    </div>
                    )
                })  
            }
        </div>
        <div  className={view ? "w-full bg-gray-50 p-1 text-lg flex flex-row space-x-1 items-stretch justify-start h-auto" : "hidden"}>
            <div className="tabs w-full" >
                <div className="tab-list flex flex-row flex-wrap space-x-1">
                    <div className="tab" onClick={()=>setTabToken(1)}>
                        <TabNormalButton index={1} token={tabToken}  label="Use Cases" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(2)} >
                        <TabNormalButton index={2} token={tabToken}  label="Add Routes" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(3)} >
                        <TabNormalButton index={3} token={tabToken}  label="Enable/Disable" />
                    </div>
                </div>
                <div className={tabToken == 1 ? "tab-panel w-full pt-5 pb-8" : "hidden"}>
                    <div className="flex w-full content-center items-center justify-center h-full ">
                    <div className="flex flex-row flex-wrap mx-5  items-stretch space-x-2 space-y-2 justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pb-2">    
                        <div></div>
                        <div className="max-w-sm rounded overflow-hidden  bg-orange-50 shadow-lg">
                          
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Adding Routes To Page</div>
                                <p className="text-gray-700 text-base">
                                The Added Routes to the Pages refers to the the api endpoints the page uses to work properly. Any thing added or removed 
                                from the page should be given careful contemplation.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded overflow-hidden bg-orange-50 shadow-lg">
                          
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Deleteing Routes From Page</div>
                                <p className="text-gray-700 text-base">
                                Deleteing Routes Needed by a page is something not recomended, as Building another page is recomended
                                for the stability of the system.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded overflow-hidden bg-orange-50 shadow-lg">
                          
                          <div className="px-6 py-4">
                              <div className="font-bold text-xl mb-2">Disable/Enable Page</div>
                              <p className="text-gray-700 text-base">
                                If the active status of Page is true, the Users that possess the required roles can access this page.
                                On the other hand, if Page is disabled, no user will be allowed to access this page
                              </p>
                          </div>
                          <div className="px-6 pt-4 pb-2">
                              <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                              <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                              <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                          </div>
                        </div>
                        
                    </div>
                    </div>
                </div>    
                <div className={tabToken == 2 ? "tab-panel w-full pt-5 pb-8" : "hidden"}>
                    <AddRouteToPageForm page_id={item.id} />
                </div>
                <div className={tabToken == 3 ? "tab-panel w-full pt-5 pb-8" : "hidden"}>
                   <ErrorBoundary>
                        { item.active ?
                        <DeactivatePageButton page_id={item.id} />:
                        <ActivatePageButton  page_id={item.id}/>
                        }
                    </ErrorBoundary>
                </div>
            </div>
        </div>
        
        
         
    </Fragment>
    )
}

function GetPagesComponent({renderData}){
    
    return (
        <>
        {
            renderData.map((x,index) =>{
                return(
                    <Fragment key={"editable"+index} >
                        <EditableGetPagesComponent  item={x} index={index} />
                    </Fragment>
                )
            })
        }
        </>
    )
}

export function EditableGetPagesComponentMobile( {item, index} ){
    const [tabToken,setTabToken]=useState(1) 
    const [view,setView]=useState(false)
    return (
        <Fragment key={'mobile-form-row'+index}>                   
            {/* # */}
            <div key={index+'-mobile'} className="w-full bg-slate-50 shadow-xl rounded-xl p-2 flex space-y-1 flex-col items-stretch justify-center" >
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 bg-gray-200 p-1">ID </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300">{item.id} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200">Name </div>
                    <div className="flex justify-center items-center w-8/12 p-1 break-all bg-gray-300">{item.name} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">App Name </div>
                    <div className="flex justify-center items-center w-8/12 break-all p-1 bg-gray-300 indent-3">{item.app}</div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">Active</div>
                    <div className="flex justify-center items-center w-8/12 break-all p-1 bg-gray-300 indent-3">{item.active.toString()}</div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1  bg-gray-200 ">Description</div>
                    <div className="flex justify-center items-center w-8/12 p-1 capitalize bg-gray-300 indent-3">{item.description}</div>
                </div>  
                <div className="w-full flex flex-row justify-center">
                <button onClick={()=>setView(!view)}>
                    { view ? <TrayUpIcon /> : <TrayDownIcon />}
                </button>
                </div> 
                <div className={ view ? "w-full flex flex-row flex-wrap justify-start" : "hidden"}>
                    {
                    item.routes.map((route,index)=>{
                        return (
                            <div key={index+route.route_path} className="flex  braek-all w-6/12 bg-slate-50 rounded-tl-lg break-all p-1 flex-row justify-start items-stretch" >
                                <div className="w-9/12 bg-slate-400 break-all p-1 break-words flex items-center justify-center">
                                    {route.route_path}
                                </div>
                                <div className="w-3/12 flex items-center justify-center bg-slate-300">
                                    <ErrorBoundary>
                                        <DeletePageRouteButton route_id={route.id} page_id={item.id} />
                                    </ErrorBoundary>
                                </div>
                            </div>
                            )
                        })  
                    }
                </div>    
                <div className={ view ? "w-full flex flex-row justify-center" : "hidden"}>
                    <div className="tabs w-full" >
                    <div className="tab-list flex flex-row flex-wrap space-x-1">
                        <div className="tab" onClick={()=>setTabToken(1)}>
                            <TabNormalButton index={1} token={tabToken}  label="Use Cases" />
                        </div>
                        <div className="tab" onClick={()=>setTabToken(2)} >
                            <TabNormalButton index={2} token={tabToken}  label="Add Routes" />
                        </div>
                    </div>
                    <div className={tabToken == 1 ? "tab-panel w-full pt-5 pb-8" : "hidden"}>
                    <div className="flex w-full content-center items-center justify-center h-full ">
                    <div className="flex flex-row flex-wrap mx-5  items-stretch space-x-2 space-y-2 justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pb-2">    
                        <div></div>
                        <div className="max-w-sm rounded overflow-hidden  bg-orange-50 shadow-lg">
                          
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Adding Routes To Page</div>
                                <p className="text-gray-700 text-base">
                                The Added Routes to the Pages refers to the the api endpoints the page uses to work properly. Any thing added or removed 
                                from the page should be given careful contemplation.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded overflow-hidden bg-orange-50 shadow-lg">
                          
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Deleteing Routes From Page</div>
                                <p className="text-gray-700 text-base">
                                Deleteing Routes Needed by a page is something not recomended, as Building another page is recomended
                                for the stability of the system.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded overflow-hidden bg-orange-50 shadow-lg">
                          
                          <div className="px-6 py-4">
                              <div className="font-bold text-xl mb-2">Disable/Enable Page</div>
                              <p className="text-gray-700 text-base">
                                If the active status of Page is true, the Users that possess the required roles can access this page.
                                On the other hand, if Page is disabled, no user will be allowed to access this page
                              </p>
                          </div>
                          <div className="px-6 pt-4 pb-2">
                              <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                              <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                              <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                          </div>
                        </div>
                        
                    </div>
                    </div>
                    </div>   
                    <div className={tabToken == 2 ? "tab-panel w-full pt-5 pb-8" : "hidden"}>
                        <AddRouteToPageForm page_id={item.id} />
                    </div>
                    </div>
                </div>
                                          
            </div>
                          
        </Fragment>
    )
}

export function GetPagesComponentMobile({renderData}){
    return(
    <>
    {
        renderData.map((x,index) =>{       
            return (
                <Fragment key={'mobile-one'+index}>
                    <EditableGetPagesComponentMobile item={x} index={index} />
                </Fragment>
            )    
        })
    }
    </>
    )
}

export function ViewPagesSection(){
    const [page, setPage]= useState(1)
    const [pageSize,setPageSize] = useState(10)
    const [sPage, setSpage]= useState(1)
    const access_token = useLogInStore((state)=>state.access_token)
    const [searchText,setSearchText] = useState('')
    
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setSearchText(value) 
    };
    
    const {data, isLoading, isSuccess, refetch} = useGetPagessHook(page,pageSize,access_token)
     if (isLoading){
        return <AppSpinner />
     }  
  
     if (isSuccess){
        let renderData;
        const sizeDropDown= Array.from({length : 50},(_,i) => i+1)
        if (searchText != ''){
            renderData=data?.data.items.filter(item => {
               return item.name.toLowerCase().includes(searchText.toLowerCase()) 
            })
        }else{
            renderData = data?.data.items
        }
         return(
        <Fragment>
            <div className="w-full flex items-stretch  justify-start h-full ">
                <div className="flex flex-col space-y-2  items-center justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pt-5 ">
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto px-4 lg:px-8 py-8 pt-0">
                        <div className="w-full h-full flex flex-row items-stretch justify-end">
                            <div className="flex justify-center items-center w-10/12 sm:w-3/12 ">
                                <input value={searchText}  onChange={handleInputChange} maxLength="50" className="rounded-lg w-9/12 text-black" placeholder="search text here .." name="search" type="search"/>
                            </div>
                            <div className="flex justify-center items-center w-10 h-full rounded bg-slate-700">
                                <button onClick={refetch}>
                                    <RefreshIcon />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto px-4 lg:px-8 py-8 pt-0">

                    <Pagination 
                    page={page} 
                    pageSize={pageSize} 
                    setPage={setPage.bind(this)} 
                    setPageSize={setPageSize.bind(this)} 
                    maxPage={data.data.pages} 
                    sPage={sPage}
                    setSpage={setSpage.bind(this)}
                    options={sizeDropDown} />
                    </div>
                    <div className="pc-block hidden md:block w-full flex-auto space-y-1  px-4 lg:px-8 py-8 pt-0">
                    <div className="w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
                            <div className="flex bg-slate-700 w-1/12 rounded-tl-lg justify-center items-center">
                                <p>ID</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-2/12">
                                <p>Name</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-3/12">
                                <p>App Name</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-2/12">
                                <p>Enabled</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-2/12">
                                <p>Description</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-1/12">
                            
                            </div>
                            <div className="flex justify-center items-center rounded-tr-lg bg-slate-700 w-1/12">
                            
                            </div>
                    </div>
                    <ErrorBoundary>
                        <GetPagesComponent renderData={renderData} />
                    </ErrorBoundary>             
                    </div>

                    <div className="mobile-block md:hidden w-full flex-auto px-4 lg:px-10 py-10 pt-0">            
                        <div className="w-full flex flex-col items-stretch h-auto space-y-1"> 
                            <ErrorBoundary>
                                <GetPagesComponentMobile renderData={renderData} />
                            </ErrorBoundary>      
                        </div>
                    </div>
                
                </div>        
            </div>
        </Fragment>
         )      
     }     
}

export function SystemPages(){
    const myContainer=useStyle((state)=>state.styles.componentWorkingDiv)   
    return(
        <div className={myContainer}>  
            <title>Pages</title>        
            <div className="bg-zinc-100 h-auto  shadow-lg w-full">
                <AddPagesForm />
            </div>
            <div className="h-full bg-zinc-100 shadow-lg w-full">
                <ErrorBoundary>
                    <ViewPagesSection />
                </ErrorBoundary>
            </div> 

        </div>
    )
    
}
 