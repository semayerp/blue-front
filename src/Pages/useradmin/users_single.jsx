import { useState, Fragment, useEffect} from "react"
import { useStyle } from "../../store/theme";
import { AppSpinner } from "../spinner";
import { useLogInStore } from "../../store/store";
import { RefreshIcon, TrashIcon, TrayDownIcon, TrayUpIcon} from "../../components/icons";
import ErrorBoundary from "../../components/errorboundary"
import { useAddUserRolesHook, useGetSingleUserHook,useGetuRolesDropDownHook, useDeleteUserRoleHook} from "../../appfetch/useradmin/users";
import { ActivateUserButton, DeactivateUserButton, ResetPasswordButton } from "./users";
import { useParams } from "react-router-dom";
import { TabNormalButton, NormalButton} from "../../components/button";
import { SelectInput } from "../../components/input";

export function RolesDropDown({name, label, uid, value, handler }){
    const access_token = useLogInStore((state)=>state.access_token)
    const { data, isSuccess, refetch } = useGetuRolesDropDownHook(access_token,uid)
    if(isSuccess) {    
        return( <>
                <SelectInput data={data.data} name={name} value={value} handler={handler} label={label} />             
                </>
            )
        }
}


function DeleteUserRoleButton({ uid , role_id}){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate } = useDeleteUserRoleHook()
    const deleteRouteRole = () => {
        
        mutate({uid, role_id, access_token})
    }    
    
    return (
        <button onClick={()=>deleteRouteRole()} >
            <TrashIcon />
        </button>
        )
}

export function AddRoleToUserForm( {uid} ){
   
    const access_token = useLogInStore((state)=>state.access_token)    
    const { mutate } = useAddUserRolesHook()
    const [value, setValue] = useState();
    const handleClick =()=>{
        if ( value != "select to add"){
            mutate({role_id:value,uid,access_token})
            
        }
    }
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setValue(value);
      
    };
 

    return(
        <Fragment>
           <div className="flex w-full content-center items-center justify-center h-full ">
                <div className="flex flex-col items-center justify-center min-w-0 break-words w-full rounded-lg  border-0">                    
                    <div className="w-full  flex-auto px-4 lg:px-10 py-10 pt-0">
                        <form className="p-5 w-full bg-sky-50 rounded-lg ">
                                <div className="flex flex-col  md:flex-row w-full" >
                                <div></div>
                                <ErrorBoundary>
                                    <RolesDropDown 
                                    name="role" 
                                    label="Role"      
                                    value={value}                                   
                                    uid={uid}
                                    handler={handleInputChange.bind(this)}
                                    />
                                </ErrorBoundary>
                                </div>                                 
                                <div className="flex flex-col md:flex-row space-x-2 w-full" > 
                                <div></div> 
                                <div className="w-full flex flex-row justify-end items-end">
                                    
                                    <NormalButton 
                                    label="Add Role" 
                                    handleClick={handleClick} />                                       
                                </div>  

                                </div>
                                
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
    )
    
}


export function EditableGetUserComponent( {item, index} ){
    const [tabToken,setTabToken]=useState(1)   
    return (
    <Fragment key={'form-row'+index}>
        <div  className="w-full  text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
            <div className="flex bg-gray-50 w-1/12 justify-center items-center">
                <p>{item.id}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-3/12 text-wrap">
                <p className="p-3">{item.uid}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-3/12 text-ellipsis">
                <p> {item.email}</p>
            </div>
            <div className="flex justify-center capitalize items-center bg-gray-50 w-1/12 ">
                <p> {item.disabled.toString()}</p>
            </div>
            <div className="flex justify-center items-center w-4/12 break-all bg-gray-50 ">
                <p className="p-3" > {item.password}</p> 
            </div>
        </div>
        <br/>
        <div className="w-full flex items-stretch justify-start h-full ">
            <div className="w-full tabs">
                <div className="tab-list flex flex-row p-1 bg-orange-50 flex-wrap space-x-1">
                    <div className="tab" onClick={()=>setTabToken(1)}>
                        <TabNormalButton index={1}  token={tabToken} label="Use Cases" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(2)}>
                        <TabNormalButton index={2} token={tabToken} label="Roles" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(3)} >
                        <TabNormalButton index={3} token={tabToken} label="Operations" />
                    </div>
                 
                </div>
                <div className={tabToken == 1 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                    <div className="flex w-full content-center items-center justify-center h-full  p-0">
                    <div className="flex flex-row flex-wrap m-1 items-stretch justify-start min-w-0 break-words w-full border-0 p-2">    
                        <div className="max-w-sm rounded m-1 overflow-hidden  bg-orange-50 shadow-lg">   
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Adding Roles To System User</div>
                                <p className="text-gray-700 text-base">
                                The Added Roles to Users refers to attaching privlege to a user to provided access to endpoints 
                                that require the specified roles
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#adduserrole</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#access</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#systemdesign</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                        
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Resetting User Password </div>
                                <p className="text-gray-700 text-base">
                                Resets Password to "default@123" and forces user to Change password when logging in.

                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                        
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Disable User </div>
                                <p className="text-gray-700 text-base">
                                If user Disabled value is "True"; it means user will not be allowed to login.
                                
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div> 
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Add Pages To Users </div>
                                <p className="text-gray-700 text-base">
                                As long as the attached Page is active;User will have Access to Operations.
                                In addtion the specified page will not be added to 
                                the user if User does not have at least one role that is required by the Page
                                for access.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Remove Pages From Users </div>
                                <p className="text-gray-700 text-base">
                                Revokes access to the specified page to User if removed.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>  
                    </div>
                    </div>
                
                </div>
                <div className={tabToken == 2 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                    <ErrorBoundary>
                    <div  className="w-full  bg-gray-50 text-lg flex flex-row flex-wrap items-stretch justify-start h-auto">
                            <div></div>
                            {
                            item.roles.map((role,index)=>{
                                return (
                                    <div key={index+role.name} className="flex w-2/12 p-1 break-all bg-slate-50 rounded-tl-lg flex-row justify-start items-stretch" >
                                        <div className="w-9/12 bg-slate-400 break-all p-1 flex items-center justify-center">
                                            <p>{role.name}</p>
                                        </div>
                                        <div className="w-3/12 flex items-center justify-center bg-slate-300">
                                            <ErrorBoundary>
                                                <DeleteUserRoleButton role_id={role.id} uid={item.uid} />
                                            </ErrorBoundary>
                                        </div>
                                    </div>
                                    )
                                })  
                            }
                    </div>
                        <br/>
                    </ErrorBoundary>
                    <ErrorBoundary>
                    <AddRoleToUserForm uid={item.uid} />
                    </ErrorBoundary>
                </div>
                <div className={tabToken == 3 ? "tab-panel w-full flex flex-row flex-wrap items-stretch justify-center pt-5 pb-8" : "hidden"} >
                    <div className="flex w-6/12 flex-col items-center justify-center h-full ">
                        <div className="flex flex-row flex-wrap mx-5  items-center space-x-2 space-y-2 justify-center min-w-0 break-words w-full rounded-lg border-0 pb-2">    
                            
                            <ErrorBoundary>
                                <ResetPasswordButton uid={item.uid} />
                            </ErrorBoundary>
                        </div>
                    </div>
                    <div className="flex w-6/12 flex-col items-center justify-center h-full ">
                        <div className="flex flex-row flex-wrap mx-5  items-center space-x-2 space-y-2 justify-center min-w-0 break-words w-full rounded-lg border-0 pb-2">    
                        
                            <ErrorBoundary>
                                { item.disabled ?
                                <ActivateUserButton  uid={item.uid}/>:
                                <DeactivateUserButton uid={item.uid} />

                                }
                            </ErrorBoundary>
                        </div>
                    </div>            
                </div>    
                
            </div> 
        </div>
        
    </Fragment>
    )
}

function GetUsersComponent({renderData}){
    return (

        <>
        
            <EditableGetUserComponent  item={renderData.data} index={1} />
                    
        </>
    )
}

export function EditableSingleUserComponentMobile( {item, index} ){
    const [tabToken,setTabToken]=useState(1) 
    const [view,setView]=useState(false)
    return (
        <Fragment key={'mobile-form-row'+index}>                   
            {/* # */}
            <div key={index+'-mobile'} className="w-full bg-slate-50 shadow-xl rounded-xl p-2 flex space-y-1 flex-col items-stretch justify-center" >
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 bg-gray-200 p-1">ID </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300">{item.id} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200">UID </div>
                    <div className="flex justify-center items-center w-8/12 p-1 break-all bg-gray-300">{item.uid} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">email </div>
                    <div className="flex justify-center items-center w-8/12 break-all p-1 bg-gray-300 indent-3">{item.email}</div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1  bg-gray-200 ">Disabled </div>
                    <div className="flex justify-center items-center w-8/12 p-1 capitalize bg-gray-300 indent-3">{item.disabled.toString()}</div>
                </div> 
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">Password </div>
                    <div className="flex justify-center items-center w-8/12 p-1 break-all bg-gray-300 indent-3">{item.password}</div>
                </div> 
                <div className="w-full flex flex-row h-10 justify-center" >
                    <button onClick={()=>setView(!view)}>
                    { view ? <TrayUpIcon /> : <TrayDownIcon /> }
                    </button>
                </div> 
            
                <div className={ view ? "w-full flex flex-row justify-center" : "hidden" }>
                <div className="w-full tabs">
                <div className="tab-list flex flex-row bg-orange-50 p-1 flex-wrap space-x-1">
                    <div className="tab" onClick={()=>setTabToken(1)}>
                        <TabNormalButton index={1}  token={tabToken} label="Use Cases" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(2)}>
                        <TabNormalButton index={2} token={tabToken} label="Roles" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(3)} >
                        <TabNormalButton index={3} token={tabToken} label="Operations" />
                    </div>
                 
                </div>
                <div className={tabToken == 1 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                <div className="flex w-full content-center items-center justify-center h-full  p-0">
                    <div className="flex flex-row flex-wrap m-1 items-stretch justify-start min-w-0 break-words w-full border-0 p-2">    
                        <div className="max-w-sm rounded m-1 overflow-hidden  bg-orange-50 shadow-lg">   
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Adding Roles To System User</div>
                                <p className="text-gray-700 text-base">
                                The Added Roles to Users refers to attaching privlege to a user to provided access to endpoints 
                                that require the specified roles
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#adduserrole</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#access</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#systemdesign</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                        
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Resetting User Password </div>
                                <p className="text-gray-700 text-base">
                                Resets Password to "default@123" and forces user to Change password when logging in.

                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded m-1 overflow-hidden bg-orange-50 shadow-lg">
                        
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Disable User </div>
                                <p className="text-gray-700 text-base">
                                If user Disabled value is "True"; it means user will not be allowed to login.
                                
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div> 
                    </div>
                    </div>
                
                </div>
                <div className={tabToken == 2 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                    <ErrorBoundary>
                    <div  className="w-full bg-gray-50 text-lg flex flex-row flex-wrap items-stretch justify-start h-auto">
                            {
                            item.roles.map((role,index)=>{
                                return (
                                    <div key={index+role.name} className="flex w-6/12 p-1 break-all bg-slate-50 rounded-tl-lg flex-row justify-start items-stretch" >
                                        <div className="w-9/12 bg-slate-400 break-all p-1 flex items-center justify-center">
                                            <p>{role.name}</p>
                                        </div>
                                        <div className="w-3/12 flex items-center justify-center bg-slate-300">
                                            <ErrorBoundary>
                                                <DeleteUserRoleButton role_id={role.id} uid={item.uid} />
                                            </ErrorBoundary>
                                        </div>
                                    </div>
                                    )
                                })  
                            }
                    </div>
                        <br/>
                    </ErrorBoundary>
                    <ErrorBoundary>
                    <AddRoleToUserForm uid={item.uid} />
                    </ErrorBoundary>
                </div>
                <div className={tabToken == 3 ? "tab-panel w-full pt-5 pb-8" : "hidden"} >
                <div className="flex w-10/12 flex-col items-center justify-center h-full ">
                        <div className="flex flex-row flex-wrap mx-5  items-center space-x-2 space-y-2 justify-center min-w-0 break-words w-full rounded-lg border-0 pb-2">    
                            
                            <ErrorBoundary>
                                <ResetPasswordButton uid={item.uid} />
                            </ErrorBoundary>
                        </div>
                    </div>
                    <div className="flex w-10/12 flex-col items-center justify-center h-full ">
                        <div className="flex flex-row flex-wrap mx-5  items-center space-x-2 space-y-2 justify-center min-w-0 break-words w-full rounded-lg border-0 pb-2">    
                        
                            <ErrorBoundary>
                                { item.disabled ?
                                <ActivateUserButton  uid={item.uid}/>:
                                <DeactivateUserButton uid={item.uid} />

                                }
                            </ErrorBoundary>
                        </div>
                    </div>            
                </div>    
                
                </div> 
                </div>                             
            </div>
                          
        </Fragment>
    )
}

export function GetSingleComponentMobile({renderData}){
    return(
    <>
        <EditableSingleUserComponentMobile item={renderData.data} index={1} />
    </>
    )
}

export function SingleUsersSection( { id }){

    const access_token = useLogInStore((state)=>state.access_token)
    const {isLoading, isSuccess, refetch,data } =useGetSingleUserHook(id,access_token)
     if (isLoading){
        return <AppSpinner />
     }
     if (isSuccess){
        let renderData = data;
             
    return(
        <Fragment>
             
            <div className="w-full flex items-stretch  justify-start h-auto  ">
                <div className="flex flex-col space-y-2  items-center justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pt-5 ">
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto px-4 lg:px-8 py-8 pt-0">
                        <div className="w-full h-full flex flex-row items-start justify-end">
                            
                            <div className="flex justify-center items-center w-8 h-8 rounded bg-slate-700">
                                <button onClick={refetch}>
                                    <RefreshIcon />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="pc-block hidden md:block"> 
                    <div className="w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
                        <div className="flex bg-slate-700 w-1/12 rounded-tl-lg justify-center items-center">
                            <p>ID</p>
                        </div>
                        <div className="flex justify-center items-center bg-slate-700 w-3/12">
                            <p>UID</p>
                        </div>
                        <div className="flex justify-center items-center bg-slate-700 w-3/12">
                        <p> Email</p>
                        </div>
                        <div className="flex justify-center items-center bg-slate-700 w-1/12">
                        <p>Disabled</p>
                        </div>
                        <div className="flex justify-center items-center w-4/12 rounded-tr-lg bg-slate-700">
                        <p> Password</p> 
                        </div>
                    </div>
                    <ErrorBoundary>
                        <GetUsersComponent renderData={renderData} />
                    </ErrorBoundary> 
                    </div>
                    <div className="mobile-block md:hidden w-full flex-auto px-4 lg:px-10 py-10 pt-0">            
                        <div className="w-full flex flex-col items-stretch h-auto space-y-1"> 
                            <ErrorBoundary>
                                <GetSingleComponentMobile renderData={renderData} />
                            </ErrorBoundary>      
                        </div>
                    </div>
                
                </div>        
            </div>
        </Fragment>
         )      
     }      
}

export function SingleUsersPage(){
    const myContainer=useStyle((state)=>state.styles.componentWorkingDiv)   
    const { id } = useParams()
    return(

        <div className={myContainer}>          
            <title>User Details</title>
            <div className="bg-zinc-100 shadow-lg h-auto pb-5 w-full ">
                <ErrorBoundary>
                    <SingleUsersSection id={id} />
                </ErrorBoundary>
            </div> 

        </div>
    )
    
}
 