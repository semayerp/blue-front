import { useState, Fragment, useEffect} from "react"
import { useStyle } from "../../store/theme";
import { AppSpinner } from "../spinner";
import { useLogInStore } from "../../store/store";
import { RefreshIcon, TrayDownIcon, TrayUpIcon} from "../../components/icons";
import ErrorBoundary from "../../components/errorboundary"
import { useGetRolesDropDownHook } from "../../appfetch/useradmin/roles";
import { useAddRouteRolesHook, useAddRoutesHook, useDeleteRouteHook, useDeleteRouteRoleHook, useGetRoutessHook } from "../../appfetch/useradmin/routes";
import { SelectInput, SingleInput } from "../../components/input";
import { NormalButton, TabNormalButton } from "../../components/button";
import { Pagination } from "../../components/pagination";
import { TrashIcon } from "../../components/icons";

export function AddRoutesForm(){
   
    const access_token = useLogInStore((state)=>state.access_token)    
    const [values, setValues] = useState({
        "name": "",
        "route_path": "",
        "description": "",
    });

    const { mutate } = useAddRoutesHook()
    const handleClick =()=>{
        console.log(values)
        mutate({values,access_token})  
    }
    
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setValues((values) => ({
           ...values,
           [target.name] : value,
        }));
       
    };


    return(
        <Fragment>
           <div className="flex w-full content-center items-center justify-center h-full ">
                <div className="flex flex-col items-center justify-center min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0">                    
                    <div className="w-full flex-auto px-4 lg:px-10 py-10 pt-0">
                        <form className="p-5 w-full">
                                <div className="flex flex-col md:flex-row space-x-2 w-full" >
                                <div></div>
                                <SingleInput 
                                name="name" 
                                label="Name"  
                                inputType="text" 
                                placeHolder="Role Name" 
                                value={values.name} 
                                handler={handleInputChange.bind(this)}  />
                                <SingleInput 
                                name="route_path" 
                                label="View Functiion"  
                                inputType="text" 
                                placeHolder="response function name here" 
                                value={values.route_path} 
                                handler={handleInputChange.bind(this)}  />
                                
                                <SingleInput 
                                name="description" 
                                label="Description"  
                                inputType="text" 
                                placeHolder="HR Administrator" 
                                value={values.description} 
                                handler={handleInputChange.bind(this)} />
                                </div>

                                <div className="flex flex-col md:flex-row space-x-2 w-full" > 
                                <div></div> 
                                <div className="w-full flex flex-col items-end">
                                    <div className="w-full sm:w-3/12">
                                    <NormalButton 
                                    label="Add End Point" 
                                    handleClick={handleClick} />     
                                    </div>
                                </div>  

                                </div>
                                
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
    )
    
}

function DeleteRouteButton({route_id}){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate } = useDeleteRouteHook()
    const deleteRoute = () => {
        
        mutate({route_id, access_token})
    }    
    
    return (
        <button onClick={()=>deleteRoute()} >
            <TrashIcon />
        </button>
        )
}


function DeleteRouteRoleButton({ route_id , role_id}){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate } = useDeleteRouteRoleHook()
    const deleteRouteRole = () => {
        
        mutate({route_id, role_id, access_token})
    }    
    
    return (
        <button onClick={()=>deleteRouteRole()} >
            <TrashIcon />
        </button>
        )
}

export function RolesDropDown({name, label, route_id, value, handler }){
    const access_token = useLogInStore((state)=>state.access_token)
    const { data, isSuccess, refetch } = useGetRolesDropDownHook(access_token,route_id)
    if(isSuccess) {    
        return( <>
                <SelectInput data={data.data} name={name} value={value} handler={handler} label={label} />  
            
                </>
            )
        }
}

export function AddRoleToRouteForm( {route_id} ){
    const access_token = useLogInStore((state)=>state.access_token)    
    const { mutate } = useAddRouteRolesHook()
    const [value, setValue] = useState();
    const handleClick =()=>{
        if ( value != "select to add"){
            mutate({role_id:value,route_id,access_token})
            
        }
    }
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setValue(value);
      
    };
 

    return(
        <Fragment>
           <div className="flex w-full content-center items-center justify-center h-full ">
                <div className="flex flex-col items-center justify-center min-w-0 break-words w-full shadow-lg rounded-lg bg-sky-50 border-0">                    
                    <div className="w-full flex-auto px-4 lg:px-10 py-10 pt-0">
                        <form className="p-5 w-full">
                                <div className="flex flex-col md:flex-row space-x-2 w-full" >
                                <div></div>
                                <ErrorBoundary>
                                    <RolesDropDown 
                                    name="role" 
                                    label="Role"      
                                    value={value}                                   
                                    route_id={route_id}
                                    handler={handleInputChange.bind(this)}
                                    />
                                </ErrorBoundary>
                                </div>                                 
                                <div className="flex flex-col md:flex-row space-x-2 w-full" > 
                                <div></div> 
                                <div className="w-full flex flex-row justify-end items-end">
                                    
                                    <NormalButton 
                                    label="Add Role" 
                                    handleClick={handleClick} />                                       
                                </div>  

                                </div>
                                
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
    )
    
}

export function EditableGetRoutesComponent( {item, index} ){
    const [tabToken,setTabToken]=useState(1) 
    const [view,setView]=useState(false)
    return (
    <Fragment key={'form-row'+index}>
        <div  className="w-full  text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
            <div className="flex bg-gray-50 w-1/12  justify-center items-center">
                <p>{item.id}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-3/12 text-wrap">
                <p className="p-3">{item.name}</p>
            </div>
            <div className="flex justify-center items-center break-all bg-gray-50 w-3/12 text-ellipsis p-3">
                <p>{item.route_path} </p>
            </div>
            <div className="flex justify-center capitalize break-all items-center bg-gray-50 w-3/12 p-3 ">
                <p> {item.description}</p>
            </div>
            <div className="flex justify-center capitalize break-all items-center bg-gray-50 w-1/12 ">
                <ErrorBoundary>
                    <DeleteRouteButton route_id={item.id} />
                </ErrorBoundary>
            </div>
            <div className="flex justify-center capitalize break-all items-center bg-gray-50 w-1/12 ">
                <button onClick={()=>setView(!view)}>
                    { view ? <TrayUpIcon /> : <TrayDownIcon />}
                </button>
            </div>
        </div>
        <div  className={view ? "w-full bg-gray-50 text-lg flex flex-row flex-wrap items-stretch justify-start h-auto" : "hidden"}>
            <div></div>
            {
            item.roles.map((role,index)=>{
                return (
                    <div key={index+role.name} className="flex w-2/12 bg-slate-50 p-1 rounded-tl-lg flex-row justify-start items-stretch" >
                        <div className="w-9/12 bg-slate-400 flex items-center justify-center">
                            <p>{role.name}</p>
                        </div>
                        <div className="w-3/12 flex items-center justify-center bg-slate-300">
                            <ErrorBoundary>
                                <DeleteRouteRoleButton role_id={role.id} route_id={item.id} />
                            </ErrorBoundary>
                        </div>
                    </div>
                    )
                })  
            }
        </div>
        <div  className={view ? "w-full bg-gray-50 p-1 text-lg flex flex-row space-x-1 items-stretch justify-start h-auto" : "hidden"}>
            <div className="tabs w-full" >
                <div className="tab-list flex flex-row flex-wrap space-x-1">
                    <div className="tab" onClick={()=>setTabToken(1)}>
                        <TabNormalButton index={1} token={tabToken}  label="Use Cases" />
                    </div>
                    <div className="tab" onClick={()=>setTabToken(2)} >
                        <TabNormalButton index={2} token={tabToken}  label="Add Role" />
                    </div>
                </div>
                <div className={tabToken == 1 ? "tab-panel w-full pt-5 pb-8" : "hidden"}>
                    <div className="flex w-full content-center items-center justify-center h-full ">
                    <div className="flex flex-row flex-wrap mx-5  items-stretch space-x-2 space-y-2 justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pb-2">    
                        <div></div>
                        <div className="max-w-sm rounded overflow-hidden  bg-orange-50 shadow-lg">
                          
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Adding Roles To API Endpoints</div>
                                <p className="text-gray-700 text-base">
                                The Added Roles to the Endpoints refers to the type of privlege that a user must have to 
                                sucessfully extract from the Endpoint. This means, if a user which does not have any of the listed priviges
                                tries to access this endpoint, it will raise 402 error.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        <div className="max-w-sm rounded overflow-hidden bg-orange-50 shadow-lg">
                          
                            <div className="px-6 py-4">
                                <div className="font-bold text-xl mb-2">Deleteing Role From Endpoint</div>
                                <p className="text-gray-700 text-base">
                                This means any user with the removed role will no longer be able to access resourses of 
                                this specfic end point.
                                </p>
                            </div>
                            <div className="px-6 pt-4 pb-2">
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                </div>    
                <div className={tabToken == 2 ? "tab-panel w-full pt-5 pb-8" : "hidden"}>
                    <AddRoleToRouteForm route_id={item.id} />
                </div>
            </div>
        </div>
        
        
         
    </Fragment>
    )
}

function GetRoutesComponent({renderData}){
    
    return (
        <>
        {
            renderData.map((x,index) =>{
                return(
                    <Fragment key={"editable"+index} >
                        <EditableGetRoutesComponent  item={x} index={index} />
                    </Fragment>
                )
            })
        }
        </>
    )
}

export function EditableGetRoutesComponentMobile( {item, index} ){
    const [tabToken,setTabToken]=useState(1) 
    const [view,setView]=useState(false)
    return (
        <Fragment key={'mobile-form-row'+index}>                   
            {/* # */}
            <div key={index+'-mobile'} className="w-full bg-slate-50 shadow-xl rounded-xl p-2 flex space-y-1 flex-col items-stretch justify-center" >
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 bg-gray-200 p-1">ID </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300">{item.id} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200">Name </div>
                    <div className="flex justify-center items-center w-8/12 p-1 break-all bg-gray-300">{item.name} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">Route Function </div>
                    <div className="flex justify-center items-center w-8/12 break-all p-1 bg-gray-300 indent-3">{item.route_path}</div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1  bg-gray-200 ">Description</div>
                    <div className="flex justify-center items-center w-8/12 p-1 capitalize bg-gray-300 indent-3">{item.description}</div>
                </div>  
                <div className="w-full flex flex-row justify-center">
                <button onClick={()=>setView(!view)}>
                    { view ? <TrayUpIcon /> : <TrayDownIcon />}
                </button>
                </div> 
                <div className={ view ? "w-full flex flex-row flex-wrap justify-start" : "hidden"}>
                    {
                    item.roles.map((role,index)=>{
                        return (
                            <div key={index+role.name} className="flex  braek-all w-6/12 bg-slate-50 rounded-tl-lg break-all p-1 flex-row justify-start items-stretch" >
                                <div className="w-9/12 bg-slate-400 break-all break-words flex items-center justify-center">
                                    {role.name}
                                </div>
                                <div className="w-3/12 flex items-center justify-center bg-slate-300">
                                    <ErrorBoundary>
                                        <DeleteRouteRoleButton role_id={role.id} route_id={item.id} />
                                    </ErrorBoundary>
                                </div>
                            </div>
                            )
                        })  
                    }
                </div>    
                <div className={ view ? "w-full flex flex-row justify-center" : "hidden"}>
                    <div className="tabs w-full" >
                    <div className="tab-list flex flex-row flex-wrap space-x-1">
                        <div className="tab" onClick={()=>setTabToken(1)}>
                            <TabNormalButton index={1} token={tabToken}  label="Use Cases" />
                        </div>
                        <div className="tab" onClick={()=>setTabToken(2)} >
                            <TabNormalButton index={2} token={tabToken}  label="Add Role" />
                        </div>
                    </div>
                    <div className={tabToken == 1 ? "tab-panel w-full pt-5 pb-8" : "hidden"}>
                        <div className="flex w-full content-center items-center justify-center h-full ">
                        <div className="flex flex-row flex-wrap mx-5  items-stretch space-x-2 space-y-2 justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pb-2">    
                            <div></div>
                            <div className="max-w-sm rounded overflow-hidden  bg-orange-50 shadow-lg">
                            
                                <div className="px-6 py-4">
                                    <div className="font-bold text-xl mb-2">Adding Roles To API Endpoints</div>
                                    <p className="text-gray-700 text-base">
                                    The Added Roles to the Endpoints refers to the type of privlege that a user must have to 
                                    sucessfully extract from the Endpoint. This means, if a user which does not have any of the listed priviges
                                    tries to access this endpoint, it will raise 402 error.
                                    </p>
                                </div>
                                <div className="px-6 pt-4 pb-2">
                                    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                                </div>
                            </div>
                            <div className="max-w-sm rounded overflow-hidden bg-orange-50 shadow-lg">
                            
                                <div className="px-6 py-4">
                                    <div className="font-bold text-xl mb-2">Deleteing Role From Endpoint</div>
                                    <p className="text-gray-700 text-base">
                                    This means any user with the removed role will no longer be able to access resourses of 
                                    this specfic end point.
                                    </p>
                                </div>
                                <div className="px-6 pt-4 pb-2">
                                    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#photography</span>
                                    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#travel</span>
                                    <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">#winter</span>
                                </div>
                            </div>
                            
                        </div>
                        </div>
                    </div>    
                    <div className={tabToken == 2 ? "tab-panel w-full pt-5 pb-8" : "hidden"}>
                        <AddRoleToRouteForm route_id={item.id} />
                    </div>
                    </div>
                </div>
                                          
            </div>
                          
        </Fragment>
    )
}

export function GetRoutesComponentMobile({renderData}){
    return(
    <>
    {
        renderData.map((x,index) =>{       
            return (
                <Fragment key={'mobile-one'+index}>
                    <EditableGetRoutesComponentMobile item={x} index={index} />
                </Fragment>
            )    
        })
    }
    </>
    )
}


export function ViewRoutesSection(){
    const [page, setPage]= useState(1)
    const [pageSize,setPageSize] = useState(25)
    const [sPage, setSpage]= useState(1)
    const access_token = useLogInStore((state)=>state.access_token)
    const [searchText,setSearchText] = useState('')
    const {data, isLoading, isSuccess, refetch} = useGetRoutessHook(page,pageSize,access_token)
    
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setSearchText(value) 
    };
 
     if (isLoading){
        return <AppSpinner />
     }  
  
     if (isSuccess){
        let renderData;
        const sizeDropDown= Array.from({length : 50},(_,i) => i+1)
        if (searchText != ''){
            renderData=data?.data.items.filter(item => {
               return item.name.toLowerCase().includes(searchText.toLowerCase()) || item.route_path.toLowerCase().includes(searchText.toLowerCase()) 
            })
        }else{
            renderData = data?.data.items
        }
         return(
        <Fragment>
            <div className="w-full flex items-stretch  justify-start h-full ">
                <div className="flex flex-col space-y-2  items-center justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pt-5 ">
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto px-4 lg:px-8 py-8 pt-0">
                        <div className="w-full h-full flex flex-row items-stretch justify-end">
                            <div className="flex justify-center items-center w-10/12 sm:w-3/12 ">
                                <input value={searchText}  onChange={handleInputChange} maxLength="50" className="rounded-lg w-9/12 text-black" placeholder="search text here .." name="search" type="search"/>
                            </div>
                            <div className="flex justify-center items-center w-10 h-full rounded bg-slate-700">
                                <button onClick={refetch}>
                                    <RefreshIcon />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto px-4 lg:px-8 py-8 pt-0">
                    <Pagination 
                    page={page} 
                    pageSize={pageSize} 
                    setPage={setPage.bind(this)} 
                    setPageSize={setPageSize.bind(this)} 
                    maxPage={data.data.pages} 
                    sPage={sPage}
                    setSpage={setSpage.bind(this)}
                    options={sizeDropDown} />
                    </div>
                    <div className="pc-block hidden md:block w-full flex-auto space-y-1  px-4 lg:px-8 py-8 pt-0">
                    <div className="w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
                            <div className="flex bg-slate-700 w-1/12 rounded-tl-lg justify-center items-center">
                                <p>ID</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-3/12">
                                <p>Name</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-3/12">
                                <p>Route Function</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-3/12">
                                <p>Description</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-1/12">
                            
                            </div>
                            <div className="flex justify-center items-center rounded-tr-lg bg-slate-700 w-1/12">
                            
                            </div>
                    </div>
                    <ErrorBoundary>
                        <GetRoutesComponent renderData={renderData} />
                    </ErrorBoundary>             
                    </div>

                    <div className="mobile-block md:hidden w-full flex-auto px-4 lg:px-10 py-10 pt-0">            
                        <div className="w-full flex flex-col items-stretch h-auto space-y-1"> 
                            <ErrorBoundary>
                                <GetRoutesComponentMobile renderData={renderData} />
                            </ErrorBoundary>      
                        </div>
                    </div>
                
                </div>        
            </div>
        </Fragment>
         )      
     }     
}

export function RoutesRoute(){
    const myContainer=useStyle((state)=>state.styles.componentWorkingDiv)   
    return(
        <div className={myContainer}>  
        <title>Endpoints</title>        
            <div className="bg-zinc-100 h-auto  shadow-lg w-full">
                <AddRoutesForm />
            </div>
            <div className="h-full bg-zinc-100 shadow-lg w-full">
                <ErrorBoundary>
                    <ViewRoutesSection />
                </ErrorBoundary>
            </div> 

        </div>
    )
    
}
 