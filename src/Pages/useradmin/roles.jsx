import { useEffect, useState, Fragment, useMemo} from "react"
import { SingleInput, ReadOnlySingleInputNoLabel, SingleInputNoLabel, TextInputNoLabel } from "../../components/input"
import { NormalButton } from "../../components/button"
import { useGetRolesHook, useAddRolesHook, useDeleteRolesHook,usePatchRolesHook} from "../../appfetch/useradmin/roles"
import { useStyle } from "../../store/theme";
import { AppSpinner } from "../spinner";
import { useLogInStore } from "../../store/store";
import { CancelIcon, EditIcon, RefreshIcon, TrashIcon, UpdateIcon } from "../../components/icons";
import { Pagination } from "../../components/pagination";
import ErrorBoundary from "../../components/errorboundary"


export function AddRolesForm(){
   
    const access_token = useLogInStore((state)=>state.access_token)    
    const [values, setValues] = useState({
        name: '',
        description : '',
    });

    const { mutate } = useAddRolesHook()
    const handleClick =()=>{
        mutate({values,access_token})  
    }
    
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setValues((values) => ({
           ...values,
           [target.name] : value,
        }));
       
    };


    return(
        <Fragment>
           <div className="flex w-full content-center items-center justify-center h-full ">
                <div className="flex flex-col items-center justify-center min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0">                    
                    <div className="w-full flex-auto px-4 lg:px-10 py-10 pt-0">
                        <form className="p-5 w-full">
                                <div className="flex flex-col md:flex-row space-x-2 w-full" >
                                <div></div>
                                <SingleInput 
                                name="name" 
                                label="Role Name"  
                                inputType="text" 
                                placeHolder="Admin" 
                                value={values.name} 
                                handler={handleInputChange.bind(this)}  />
                                <SingleInput 
                                name="description" 
                                label="Description"  
                                inputType="text" 
                                placeHolder="HR Administrator" 
                                value={values.description} 
                                handler={handleInputChange.bind(this)} />
                                </div>                                 
                                <div className="flex flex-col md:flex-row space-x-2 w-full" > 
                                <div></div> 
                                <div className="w-full flex flex-col items-end">
                                    <div className="w-full sm:w-3/12">
                                    <NormalButton 
                                    label="Add Role" 
                                    handleClick={handleClick} />     
                                    </div>
                                </div>  

                                </div>
                                
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
    )
    
}


function DeleteRoleButton(role_id){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate } = useDeleteRolesHook()
    const deleteRole = (role_id) => {
        mutate({role_id :role_id.role_id, access_token})
    }    
    
    return (
        <button onClick={()=>deleteRole(role_id)} >
            <TrashIcon />
        </button>
        )
}


function PatchRoleButton({ patch_data }){
    const access_token = useLogInStore((state)=>state.access_token)
    const { mutate,isSuccess,reset } = usePatchRolesHook()
    const patchRole = () => { 
        mutate({patch_data, access_token})
    }    
    if(isSuccess){
    
        reset()
    }else{
        return (
            <button onClick={()=>patchRole()} >
                <UpdateIcon />
            </button>
            )  
    }

}  


export function EditableGetRolesComponent( {item, index} ){
    const [edit,setEdit]=useState(false)
    const [values, setValues] = useState({
        id : item.id,
        name: item.name,
        description : item.description,
    });
    
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setValues((values) => ({
           ...values,
           [target.name] : value,
        }));
    };
    return (
    <Fragment key={'form-row'+index}>
        <div  className={!edit ? "w-full  text-lg flex flex-row space-x-1 items-stretch justify-center h-auto" : "hidden"}>
            <div className="flex bg-gray-50 w-1/12 justify-center items-center">
                <p>{item.id}</p>
            </div>
            <div className="flex justify-center items-center bg-gray-50 w-4/12">
                <p>{item.name}</p>
            </div>
            <div className="flex p-5 indent-3 justify-center items-center bg-gray-50 w-5/12">
            <p> {item.description}</p>
            </div>
            <div className="flex justify-center items-center w-1/12 bg-gray-50">
                <button onClick={()=>setEdit(!edit)}>
                    <EditIcon />
                </button>
            </div>
            <div className="flex justify-center items-center w-1/12 bg-gray-50">
                <ErrorBoundary>
                    <DeleteRoleButton role_id={item.id}/>
                </ErrorBoundary>
            </div>
        </div>
        <div  className={edit ? "w-full  text-lg flex flex-row space-x-1 items-stretch justify-center h-auto" : "hidden"}>
            <div className="flex bg-gray-50 w-1/12 justify-center items-center">
            <ReadOnlySingleInputNoLabel 
                    name="id" 
                    label="Role Name"  
                    inputType="text" 
                    placeHolder="Admin" 
                    value={values.id} 
                        />
            </div>
            <div className="flex justify-center items-center bg-gray-50 w-4/12">
            <SingleInputNoLabel 
                    name="name" 
                    label="Role Name"  
                    inputType="text" 
                    placeHolder="Admin" 
                    value={values.name} 
                    handler={handleInputChange.bind(this)}  />
            </div>
            <div className="flex p-5 indent-3 justify-center items-center bg-gray-50 w-5/12">
            <TextInputNoLabel 
                    name="description" 
                    label="Description"  
                    inputType="text" 
                    placeHolder="HR Administrator" 
                    value={values.description} 
                    handler={handleInputChange.bind(this)} />
            </div>
            <div className="flex justify-center items-center w-1/12 bg-gray-50">
                <ErrorBoundary>
                    <PatchRoleButton patch_data={values} />
                </ErrorBoundary>
            </div>
            <div className="flex justify-center items-center w-1/12 bg-gray-50">
                <button onClick={()=>setEdit(!edit)} >
                    <CancelIcon />
                </button>                            
            </div>
        </div>        
    </Fragment>
    )
}


function GetRolesComponent({renderData}){
    
    return (
        <>
        {
            renderData.map((x,index) =>{
                return(
                    <Fragment key={"editable"+index} >
                    <EditableGetRolesComponent  item={x} index={index} />
                    </Fragment>
                )
            })
        }
        </>
    )
}


export function EditableGetRolesComponentMobile( {item, index} ){
    const [edit,setEdit]=useState(false)
    const [values, setValues] = useState({
        id : item.id,
        name: item.name,
        description : item.description,
    });
    
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setValues((values) => ({
           ...values,
           [target.name] : value,
        }));
       
    };
    return (
        <Fragment key={'mobile-form-row'+index}>                   
            {/* # */}
            <div key={index+'-mobile'} className={!edit ? "w-full bg-slate-50 shadow-xl rounded-xl p-2 flex space-y-1 flex-col items-stretch justify-center" : "hidden"}>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 bg-gray-200 p-1">ID </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300">{item.id} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200">Role </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300">{item.name} </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">Description </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300 indent-3">{item.description}</div>
                </div> 
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-1/2 p-1 bg-gray-100 ">
                        <button onClick={()=>setEdit(!edit)} >
                            <EditIcon />
                        </button> 
                    </div>
                    <div className="flex justify-center items-center w-1/2 p-1 bg-gray-100 indent-3">
                        <ErrorBoundary>
                            <DeleteRoleButton role_id={item.id}/>
                        </ErrorBoundary>
                        </div>
                </div>                             
            </div>
            {/* ## */}
            <div key={index+'-mobile-form'} className={edit ? "w-full bg-slate-50 shadow-xl rounded-xl p-2 flex space-y-1 flex-col items-stretch justify-center" : "hidden"}>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 bg-gray-200 p-1">ID </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300"> 
                    <ReadOnlySingleInputNoLabel 
                        name="id" 
                        label="Role Name"  
                        inputType="text" 
                        placeHolder="Admin" 
                        value={values.id} 
                            /> 
                    </div>
                </div>
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200">Role </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300">
                    <SingleInputNoLabel 
                        name="name" 
                        label="Role Name"  
                        inputType="text" 
                        placeHolder="Admin" 
                        value={values.name} 
                        handler={handleInputChange.bind(this)}  />
                    </div>
                </div>
                <div className="w-full flex flex-row  justify-center">
                    <div className="flex justify-center items-center w-4/12 p-1 bg-gray-200 ">Description </div>
                    <div className="flex justify-center items-center w-8/12 p-1 bg-gray-300">
                    <TextInputNoLabel 
                        name="description" 
                        label="Description"  
                        inputType="text" 
                        placeHolder="HR Administrator" 
                        value={values.description} 
                        handler={handleInputChange.bind(this)} />
                    </div>
                </div> 
                <div className="w-full flex flex-row justify-center">
                    <div className="flex justify-center items-center w-1/2 p-1 bg-gray-100 ">
                        <ErrorBoundary>
                            <PatchRoleButton patch_data={values} />
                        </ErrorBoundary>
                    </div>
                    <div className="flex justify-center items-center w-1/2 p-1 bg-gray-100 indent-3">
                        <button onClick={()=>setEdit(!edit)} >
                            <CancelIcon />
                        </button>   
                    </div>
                </div>                             
            </div>
            {/* # */}               
        </Fragment>
    )
}


export function GetRolesComponentMobile({renderData}){
    return(
    <>
    {
        renderData.map((x,index) =>{       
            return (
                <Fragment key={'mobile-one'+index}>
                    <EditableGetRolesComponentMobile item={x} index={index} />
                </Fragment>
            )    
        })
    }
    </>
    )
}


export function ViewRolesSection(){
    const [page, setPage]= useState(1)
    const [pageSize,setPageSize] = useState(10)
    const [sPage, setSpage]= useState(1)
    const access_token = useLogInStore((state)=>state.access_token)
    const [searchText,setSearchText] = useState('')
    
    const handleInputChange = (event) => {
        event.persist();
        const target=event.target
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setSearchText(value) 
    };
    
    const {data, isLoading, isSuccess, refetch} =useGetRolesHook(page,pageSize,access_token)
     if (isLoading){
        return <AppSpinner />
     }  
  
     if (isSuccess){
        let renderData;
        const sizeDropDown= Array.from({length : 50},(_,i) => i+1)
        if (searchText != ''){
            renderData=data?.data.items.filter(item => {
               return item.description.toLowerCase().includes(searchText.toLowerCase()) || item.name.toLowerCase().includes(searchText.toLowerCase())
            })
        }else{
            renderData = data?.data.items
        }
         return(
        <Fragment>
            <div className="w-full flex items-stretch  justify-start h-full ">
                <div className="flex flex-col space-y-2  items-center justify-start min-w-0 break-words w-full shadow-lg rounded-lg bg-gray-200 border-0 pt-5 ">
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto px-4 lg:px-8 py-8 pt-0">
                        <div className="w-full h-full flex flex-row items-stretch justify-end">
                            <div className="flex justify-center items-center w-10/12 sm:w-3/12 ">
                                <input value={searchText}  onChange={handleInputChange} maxLength="50" className="rounded-lg w-9/12 text-black" placeholder="search text here .." name="search" type="search"/>
                            </div>
                            <div className="flex justify-center items-center w-10 h-full rounded bg-slate-700">
                                <button onClick={refetch}>
                                    <RefreshIcon />
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="search bar w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto px-4 lg:px-8 py-8 pt-0">
                    <Pagination 
                    page={page} 
                    pageSize={pageSize} 
                    setPage={setPage.bind(this)} 
                    setPageSize={setPageSize.bind(this)} 
                    maxPage={data.data.pages} 
                    sPage={sPage}
                    setSpage={setSpage.bind(this)}
                    options={sizeDropDown} />
                    </div>
                    <div className="pc-block hidden md:block w-full flex-auto space-y-1  px-4 lg:px-8 py-8 pt-0">
                    <div className="w-full  text-white text-lg flex flex-row space-x-1 items-stretch justify-center h-auto">
                            <div className="flex bg-slate-700 w-1/12 rounded-tl-lg justify-center items-center">
                                <p>ID</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-4/12">
                                <p>Role Name</p>
                            </div>
                            <div className="flex justify-center items-center bg-slate-700 w-5/12">
                            <p> Descriptions</p>
                            </div>
                            <div className="flex justify-center items-center w-1/12 bg-slate-700">
                            </div>
                            <div className="flex justify-center items-center w-1/12 rounded-tr-lg bg-slate-700">
                            </div>
                    </div>
                    <ErrorBoundary>
                        <GetRolesComponent renderData={renderData} />
                    </ErrorBoundary>             
                    </div>

                    <div className="mobile-block md:hidden w-full flex-auto px-4 lg:px-10 py-10 pt-0">            
                        <div className="w-full flex flex-col items-stretch h-auto space-y-1"> 
                            <ErrorBoundary>
                                <GetRolesComponentMobile renderData={renderData} />
                            </ErrorBoundary>      
                        </div>
                    </div>
                
                </div>        
            </div>
        </Fragment>
         )      
     }     
}


export function RolesPage(){
    const myContainer=useStyle((state)=>state.styles.componentWorkingDiv)   
    return(

        <div className={myContainer}>
            <title>Roles</title>          
            <div className="bg-zinc-100 h-auto  shadow-lg w-full">
                <AddRolesForm />
            </div>
            <div className="h-full bg-zinc-100 shadow-lg w-full">
                <ErrorBoundary>
                    <ViewRolesSection />
                </ErrorBoundary>
            </div> 

        </div>
    )
    
}
 