import {  useEffect, useState, forwardRef } from 'react'
import { useStyle } from '../store/theme'
import { useLogInStore } from '../store/store'
import { Link, Outlet } from 'react-router-dom'
import { client } from '../appfetch/main'
import { useGetMenueHook } from '../appfetch/useradmin/sidebar'
import { ErrorBoundary } from 'react-error-boundary'
import { Fragment } from 'react'
import { Menu, Transition } from '@headlessui/react'
import { ThreeBarsIcon } from '../components/icons'


function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

// export function SideMenu(){

//     const dashDivClass=useStyle((state)=> state.styles.sideBarNav)
//     const authToken=useLogInStore((state)=> state.access_token)
//     const { data, isSuccess } = useGetMenueHook(authToken)
 
//     const data2={ title:"Log In",
//             links:[ ['/login','Log In']]
//         }
//     if(isSuccess){      
//     const myData = data.data
//     const appList = Object.keys(data.data)
    
//     return(
//         <div className={dashDivClass}>
//         {
//             authToken  ? 
//             <>
                
//             {
//             appList.map((value)=>{
//                     return(
//                         <DropDownMenue key={"d"+value} data={myData[value]} title={value} />
//                     )
//                 })
            
//             }    
//             </>
//                 :
//             <DropDownMenue data={data2.links}  title={data2.title} />
//         }

//        </div>    )
//     }
// }

const Cart = forwardRef((props, forwardedRef) => {
    return (
      <Fragment ref={forwardedRef}>
        {props.children}
      </Fragment>
    )
  })
export function AppMenue() {

    const authToken=useLogInStore((state)=> state.access_token)
    const { data, isSuccess } = useGetMenueHook(authToken)
    const logout=useLogInStore((state)=> state.resetTokenLogout)
    const handleLogOut=()=>{
        client.defaults.headers.common.Authorization = `Bearer token`
        logout()
    }    
    if(isSuccess){
    const myData = data.data
    const appList = Object.keys(data.data) 
        return (
        < div className='lg:hidden w-full flex items-center justify-end markb' >  
        <Menu as="div" className="lg:hidden text-left flex-col items-end justify-end ">
            <div className='w-full flex items-center justify-end'>
            <Menu.Button className="flex w-full justify-end gap-x-1.5 rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50">
                Menue
                <ThreeBarsIcon />
            </Menu.Button>
            </div>
            <Cart>
    
            <Transition
           
            as={Fragment}
            enter="transition ease-out duration-100"
            enterFrom="transform opacity-0 scale-95"
            enterTo="transform opacity-100 scale-100"
            leave="transition ease-in duration-75"
            leaveFrom="transform opacity-100 scale-100"
            leaveTo="transform opacity-0 scale-95"
            >
            { authToken ?

            <Menu.Items className="absolute right-3 z-50 mt-2 w-56 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
                {  
                  appList.map((value,index)=>{
                    
                return( 
                <div key={index+value} className="py-1">
                    
                    { myData[value].map((x,index)=>{
                        
                        return(
                        <Menu.Item  key={x[0]}  as={Fragment}>     
                        {({ active }) => (
                            <a
                             
                            href={x[1] == "Logout" ? "#" : x[0]}
                            onClick={x[1] == "Logout" ? handleLogOut : null}
                            className={classNames(
                                active ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                                'block px-4 py-2 text-sm'
                                )}
                                >
                            {x[1]}
                            </a>
                        )}
                        </Menu.Item>
                        )
                        })
                        }
                   
                </div>
                    )
                })  
                
                
                }
            </Menu.Items>
              :
              ""
              
            } 
            
            </Transition>
            </Cart>
        </Menu>
        </div>
    )
}    
}
  