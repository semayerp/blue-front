import { useStore } from '../store/store'
import { LogInPage } from '../Pages/useradmin/login'
import { PrivateRoutes } from '../components/privateroute'
import { FormLayOutPage } from '../Pages/viewlayout'
import { AppSpinner } from '../Pages/spinner'
import { useStyle } from '../store/theme'
import { TestCont } from '../sections/testcont'
import { Routes, Route } from 'react-router-dom'
import { RolesPage } from '../Pages/useradmin/roles'
import { UsersPage } from '../Pages/useradmin/users'
import { RegisterPage } from '../Pages/useradmin/register'
import { SingleUsersPage } from '../Pages/useradmin/users_single'
import { RoutesRoute } from '../Pages/useradmin/routes'
import { SystemPages } from '../Pages/useradmin/pages'
import { EmployeesPage } from '../Pages/hrm/employee'
import { SingleEmployeePage } from '../Pages/hrm/employee_single'
import { BanksPage } from '../Pages/hrm/banks'
function Home(){
    const compDivClass=useStyle((state)=>state.bushuComponentContainer)
   
    return(
        <div className={compDivClass}>
            <h1>
                This is Home Page       
            </h1>
       

        </div>
    )
}


export function MainContent(){
    const contDivClass=useStyle((state)=> state.styles.bushuContSec)
    return(
    <div className={contDivClass}>
       
        
        <Routes>
                <Route element={<PrivateRoutes /> }>  
                    <Route path="/"     element={<Home />} />
                    <Route path="/spin" element={<AppSpinner />} />  
                    <Route path="/layout" element={<FormLayOutPage />} />  
                    <Route path="/roles" element={<RolesPage />} />  
                    <Route path="/users" element={<UsersPage /> } />
                    <Route path="/routes" element={<RoutesRoute />} />
                    <Route path="/pages" element={<SystemPages /> } />
                    <Route path="/banks" element={<BanksPage /> } />
                    <Route path="/users/:id" element={<SingleUsersPage /> } />
                    <Route path="/employee" element={<EmployeesPage /> } />
                    <Route path="/employee/:id" element={<SingleEmployeePage /> } />
                </Route>
            <Route path="/register" element={<RegisterPage /> } />    
            <Route path="/login" element={<LogInPage/>} />   
        </Routes>
    
     </div>
    )
}