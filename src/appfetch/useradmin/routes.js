import { client } from "../main";
import { useQuery, useMutation, useQueryClient } from 'react-query'
import toast from 'react-hot-toast'

// ## end of chunck
//  start of chunck
export const getRoutesAdmin = async ({ queryKey }) => {
    const page = queryKey[1]
    const pageSize = queryKey[2]
    const access_token = queryKey[3]
    return await client({
        method: 'GET',
        url: `/useradmin/response_path?page=${page ? page : 1}&size=${pageSize ? pageSize : 20}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data?.items)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response?.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetRoutessHook =(page,pageSize,access_token)=>{
    
	return useQuery(
        ['get-routes',page,pageSize,access_token], getRoutesAdmin,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}

// ## end of chunck
//  start of chunck
export const addRoutesAdmin = async (data) => {
    return await client({
        method: 'POST',
        url: `/useradmin/response_path`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        data : data.values,
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Added User Sucessfully',{
                position: 'top-right'
              })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    

export const useAddRoutesHook =(data,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
                mutationFn : (data)=> addRoutesAdmin(data),
                enabled: false,            
                onSuccess : ()=>{
                    queryClient.invalidateQueries('get-routes')
                },
                onSettled: (data) => {
                return data?.response?.data
                    // console.log(data?.response?.status)
                }
            }
              )
    }

// ## end of chunck
//  start of chunck
export const addRouteRolesAdmin = async (data) => {
    return await client({
        method: 'POST',
        url: `/useradmin/response_path/${data.route_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        data : {role_id : data.role_id },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Added User Sucessfully',{
                position: 'top-right'
              })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    

export const useAddRouteRolesHook =(data,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
                mutationFn : (data)=> addRouteRolesAdmin(data),
                enabled: false,            
                onSuccess : ()=>{
                    queryClient.invalidateQueries('get-routes')
                },
                onSettled: (data) => {
                return data?.response?.data
                    // console.log(data?.response?.status)
                }
            }
              )
    }

// ## end of chunk
// start of chunk

// ## end of chunck 
// start of chunck  
       
// ## end of chunck
//  start of chunck

    
// ## end of chunck
//  start of chunck
export const deleteRouteAdmin = async (data) => {
    console.log(data)
    return await client({
        method: 'DELETE',
        url: `/useradmin/response_path/${data.route_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Removed User Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
    
} 


export const useDeleteRouteHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> deleteRouteAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
            queryClient.invalidateQueries('get-routes')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}
// ## end of chunck
//  start of chunck
export const deleteRouteRoleAdmin = async (data) => {
    return await client({
        method: 'DELETE',
        url: `/useradmin/response_path/${data.route_id}/${data.role_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Removed User Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
    
} 


export const useDeleteRouteRoleHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> deleteRouteRoleAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
        queryClient.invalidateQueries('get-routes')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}

// ## end of chunck