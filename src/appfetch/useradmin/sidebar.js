import { client } from "../main";
import { useQuery, useMutation, useQueryClient } from 'react-query'

export const getAdminMenue = async ({ queryKey }) => {
    const access_token = queryKey[1]
    return await client({
        method: 'GET',
        url: `/useradmin/dashboard`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            return response
          
        }).catch((response,error)=> {
            
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetMenueHook =(access_token)=>{
    
	return useQuery(
        ['get-menue',access_token], getAdminMenue,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
		return data?.response?.data
		
		}
        }
          )
}

