import { client } from "../main";
import { useQuery, useMutation, useQueryClient } from 'react-query'
import toast from 'react-hot-toast'

// ## end of chunck
//  start of chunck
export const getPagesAdmin = async ({ queryKey }) => {
    const page = queryKey[1]
    const pageSize = queryKey[2]
    const access_token = queryKey[3]
    return await client({
        method: 'GET',
        url: `/useradmin/page?page=${page ? page : 1}&size=${pageSize ? pageSize : 20}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data?.items)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetPagessHook =(page,pageSize,access_token)=>{
    
	return useQuery(
        ['get-pages',page,pageSize,access_token], getPagesAdmin,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}

// ## end of chunck
//  start of chunck
export const addPagesAdmin = async (data) => {
    return await client({
        method: 'POST',
        url: `/useradmin/page`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        data : data.values,
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Added Page Sucessfully',{
                position: 'top-right'
              })
            return response
          
        }).catch((response,error)=> {
            console.log(response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    

export const useAddPagesHook =(data,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
                mutationFn : (data)=> addPagesAdmin(data),
                enabled: false,            
                onSuccess : ()=>{
                    queryClient.invalidateQueries('get-pages')
                },
                onSettled: (data) => {
                return data?.response?.data
                    // console.log(data?.response?.status)
                }
            }
              )
    }

// ## end of chunck
//  start of chunck
export const addPageRouteAdmin = async (data) => {
    return await client({
        method: 'POST',
        url: `/useradmin/page/${data.page_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        data : {route_id : data.route_id },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Added Route to Page Sucessfully',{
                position: 'top-right'
              })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    

export const useAddPageRouteHook =(data,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
                mutationFn : (data)=> addPageRouteAdmin(data),
                enabled: false,            
                onSuccess : ()=>{
                    queryClient.invalidateQueries('get-pages')
                },
                onSettled: (data) => {
                return data?.response?.data
                    // console.log(data?.response?.status)
                }
            }
              )
    }

// ## end of chunk
// start of chunk
export const patchPageActivateAdmin = async (data) => {
    
    return await client({
        method: 'PATCH',
        url: `/useradmin/activatepage/${data.page_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Enabled Page Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
        
    } 
  
export const usePatchActivatePageHook =(page_id,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
            mutationFn : (data)=> patchPageActivateAdmin(data),
            enabled: false,            
            onSuccess : ()=>{
                queryClient.invalidateQueries('get-pages')
            },
            onSettled: (data) => {
            return data?.response?.data
                // console.log(data?.response?.status)
            }
            }
                )
    }
    
// ## end of chunck 
// start of chunck  
export const patchPageDeactivateAdmin = async (data) => {
        
        return await client({
            method: 'PATCH',
            url: `/useradmin/deactivatepage/${data.page_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${data.access_token}`,
            },
            
        }).then(function (response) {
                // console.log(response?.data?.items)
                toast.success('Diabled Page Sucessfully',{
                    position: 'top-right'
                    })
                return response
                
            }).catch((response,error)=> {
                console.log(response?.response)
                if (response?.response.status == 422){
                    toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                        position: 'top-right'
                        })
                } else {
                toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                    position: 'top-right'
                    })
                }
                return response
            });
            
        } 

export const usePatchDeactivatePageHook =(page_id,access_token)=>{
            const queryClient = useQueryClient()
            return useMutation(
                {
                mutationFn : (data)=> patchPageDeactivateAdmin(data),
                enabled: false,            
                onSuccess : ()=>{
                    queryClient.invalidateQueries('get-pages')
                },
                onSettled: (data) => {
                return data?.response?.data
                    // console.log(data?.response?.status)
                }
                }
                    )
        }   
// ## end of chunck 
// start of chunck  
       
// ## end of chunck
//  start of chunck

    
// ## end of chunck
//  start of chunck
export const deletePageAdmin = async (data) => {
    return await client({
        method: 'DELETE',
        url: `/useradmin/page/${data.page_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Removed Page Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
    
} 


export const useDeletePageHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> deletePageAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
            queryClient.invalidateQueries('get-pages')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}
// ## end of chunck
//  start of chunck
export const deletePageRouteAdmin = async (data) => {
    return await client({
        method: 'DELETE',
        url: `/useradmin/page/${data.page_id}/${data.route_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Removed Route Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
    
} 


export const useDeletePageRouteHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> deletePageRouteAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
        queryClient.invalidateQueries('get-pages')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}



// ## end of chunck