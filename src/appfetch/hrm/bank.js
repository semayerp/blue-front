import { client } from "../main";
import { useQuery, useMutation, useQueryClient } from 'react-query'
import toast from 'react-hot-toast'

// start of chunk
export const getBanksAdmin = async ({ queryKey }) => {
    const page = queryKey[1]
    const pageSize = queryKey[2]
    const access_token = queryKey[3]
    return await client({
        method: 'GET',
        url: `/hrm/bank?page=${page ? page : 1}&size=${pageSize ? pageSize : 20}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data?.items)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetBanksHook =(page,pageSize,access_token)=>{
    
	return useQuery(
        ['get-banks',page,pageSize,access_token],getBanksAdmin,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}
// end of chunk
// start of chunk

export const addBankAdmin = async (data) => {
    
    return await client({
        method: 'POST',
        url: `/hrm/bank`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        data : data.values,
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Added Bank Sucessfully',{
                position: 'top-right'
              })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 


export const useAddBanksHook =(data,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
            mutationFn : (data)=> addBankAdmin(data),
            enabled: false,            
            onSuccess : ()=>{
                queryClient.invalidateQueries('get-banks')
            },
            onSettled: (data) => {
            return data?.response?.data
                // console.log(data?.response?.status)
            }
            }
              )
    }
// end of chunk
// start of chunk


export const patchBankAdmin = async (data) => {

return await client({
    method: 'PATCH',
    url: `/hrm/bank`,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${data.access_token}`,
    },
    data : data.patch_data,
}).then(function (response) {
        // console.log(response?.data?.items)
        toast.success('Updated Bank Sucessfully',{
            position: 'top-right'
            })
        return response
        
    }).catch((response,error)=> {
        console.log(response?.response)
        if (response?.response.status == 422){
            toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                position: 'top-right'
                })
        } else {
        toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
            position: 'top-right'
            })
        }
        return response
    });
    
} 


export const usePatchBanksHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> patchBankAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
            queryClient.invalidateQueries('get-banks')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}
// end of chunk
// start of chunk

export const deleteRolesAdmin = async (data) => {
    return await client({
        method: 'DELETE',
        url: `/useradmin/roles/${data.role_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Removed Role Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
    
} 


export const useDeleteRolesHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> deleteRolesAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
            queryClient.invalidateQueries('get-roles')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}
// end of chunk
// start of chunk

export const getRolesDropDownAdmin = async ({ queryKey }) => {
    const access_token = queryKey[1]
    const route_id = queryKey[2]   
    return await client({
        method: 'GET',
        url: `/useradmin/dropdown?route_id=${route_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetRolesDropDownHook =(access_token,route_id, user_id)=>{
    const queryClient = useQueryClient()
	return useQuery(
        ['get-dropdown',access_token, route_id],getRolesDropDownAdmin,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
    
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}

// ## end of chunck
//  start of chunck
export const getRoutesDropDownAdmin = async ({ queryKey }) => {
    const access_token = queryKey[1]
    const page_id = queryKey[2]
    return await client({
        method: 'GET',
        url: `/useradmin/routedropdown?page_id=${page_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetRouteDropDownHook =(access_token, page_id)=>{
	return useQuery(
        ['get-route-dropdown',access_token, page_id],getRoutesDropDownAdmin,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
    
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}

// end of chunk