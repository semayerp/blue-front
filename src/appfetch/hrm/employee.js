import { client } from "../main";
import { useQuery, useMutation, useQueryClient } from 'react-query'
import toast from 'react-hot-toast'

// ## end of chunck
//  start of chunck
export const getEmployeesAdmin = async ({ queryKey }) => {
    const page = queryKey[1]
    const pageSize = queryKey[2]
    const access_token = queryKey[3]
    return await client({
        method: 'GET',
        url: `/hrm/employee?page=${page ? page : 1}&size=${pageSize ? pageSize : 20}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data?.items)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetEmployeesHook =(page,pageSize,access_token)=>{
    
	return useQuery(
        ['get-employees',page,pageSize,access_token], getEmployeesAdmin,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}

// ## end of chunck
//  start of chunck
export const addEmployeeAdmin = async (data) => {
    console.log(data)
    return await client({
        method: 'POST',
        url: `/hrm/employee`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        data : data,
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Added Employee Sucessfully',{
                position: 'top-right'
              })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    

export const useAddEmployeeHook =(data,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
            mutationFn : (data)=> addEmployeeAdmin(data),
            enabled: false,            
            onSuccess : ()=>{
                queryClient.invalidateQueries('get-employees')
            },
            onSettled: (data) => {
            return data?.response?.data
                // console.log(data?.response?.status)
            }
            }
              )
    }

// ## end of chunck
//  start of chunck
export const patchEmployeeAdmin = async (data) => {
    return await client({
        method: 'PATCH',
        url: `/hrm/employee`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        data : data.data,
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Updated Employee Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
    
} 


export const usePatchEmployeeHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> patchEmployeeAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
            queryClient.invalidateQueries('get-employees')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}

// ## end of chunk
// start of chunk
export const patchUserActivateAdmin = async (data) => {
    
    return await client({
        method: 'PATCH',
        url: `/useradmin/activate/${data.uid}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Activated User Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
        
    } 
  
export const usePatchActivateUserHook =(uid,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
            mutationFn : (data)=> patchUserActivateAdmin(data),
            enabled: false,            
            onSuccess : ()=>{
                queryClient.invalidateQueries('get-users')
            },
            onSettled: (data) => {
            return data?.response?.data
                // console.log(data?.response?.status)
            }
            }
                )
    }
    
// ## end of chunck 
// start of chunck  
export const patchUserDeactivateAdmin = async (data) => {
        
        return await client({
            method: 'PATCH',
            url: `/useradmin/deactivate/${data.uid}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${data.access_token}`,
            },
            
        }).then(function (response) {
                // console.log(response?.data?.items)
                toast.success('Deactivated User Sucessfully',{
                    position: 'top-right'
                    })
                return response
                
            }).catch((response,error)=> {
                console.log(response?.response)
                if (response?.response.status == 422){
                    toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                        position: 'top-right'
                        })
                } else {
                toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                    position: 'top-right'
                    })
                }
                return response
            });
            
        } 

export const usePatchDeactivateUserHook =(uid,access_token)=>{
            const queryClient = useQueryClient()
            return useMutation(
                {
                mutationFn : (data)=> patchUserDeactivateAdmin(data),
                enabled: false,            
                onSuccess : ()=>{
                    queryClient.invalidateQueries('get-users')
                },
                onSettled: (data) => {
                return data?.response?.data
                    // console.log(data?.response?.status)
                }
                }
                    )
        }        
// ## end of chunck
//  start of chunck
export const patchResetPassword = async (data) => {
    
    return await client({
        method: 'PATCH',
        url: `/useradmin/password/${data.uid}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Password Reset Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
        
    } 
    
    
export const usePatchResetPasswordHook =(uid,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
            mutationFn : (data)=> patchResetPassword(data),
            enabled: false,            
            onSuccess : ()=>{
                queryClient.invalidateQueries('get-users')
               
            },
            onSettled: (data) => {
            return data?.response?.data
                // console.log(data?.response?.status)
            }
            }
                )
    }
    
    
// ## end of chunck
//  start of chunck
export const deleteUserAdmin = async (data) => {
    return await client({
        method: 'DELETE',
        url: `/useradmin/user/${data.uid}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Removed User Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
    
} 


export const useDeleteUserHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> deleteUserAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
            queryClient.invalidateQueries('get-users')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}
// ## end of chunck
//  start of chunck
export const getSingleUser = async ({ queryKey }) => {
    const id = queryKey[1]
    const access_token = queryKey[2]
    return await client({
        method: 'GET',
        url: `/useradmin/user/${id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetSingleUserHook =(id, access_token)=>{
    
	return useQuery(
        ['get-single-user',id, access_token], getSingleUser,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}
// ## end of chunck
//  start of chunck
export const addUserRolesAdmin = async (data) => {
    return await client({
        method: 'POST',
        url: `/useradmin/roles/${data.uid}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        data : {role_id : data.role_id },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Added Role to User Sucessfully',{
                position: 'top-right'
              })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    

export const useAddUserRolesHook =(data,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
                mutationFn : (data)=> addUserRolesAdmin(data),
                enabled: false,            
                onSuccess : ()=>{
                    queryClient.invalidateQueries('get-users')
                },
                onSettled: (data) => {
                return data?.response?.data
                    // console.log(data?.response?.status)
                }
            }
              )
    }

// ## end of chunck
//  start of chunck
export const addUserPagesAdmin = async (data) => {
    return await client({
        method: 'POST',
        url: `/useradmin/user/${data.user_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
        data : {page_id : data.page_id },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Added Page to User Sucessfully',{
                position: 'top-right'
              })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    

export const useAddUserPagesHook =(data,access_token)=>{
        const queryClient = useQueryClient()
        return useMutation(
            {
                mutationFn : (data)=> addUserPagesAdmin(data),
                enabled: false,            
                onSuccess : ()=>{
                    queryClient.invalidateQueries('get-user-pages')
                },
                onSettled: (data) => {
                return data?.response?.data
                    // console.log(data?.response?.status)
                }
            }
              )
    }

// ## end of chunck
//  start of chunck

export const getuRolesDropDownAdmin = async ({ queryKey }) => {
    const access_token = queryKey[1]
    const uid = queryKey[2]
    return await client({
        method: 'GET',
        url: `/useradmin/dropdown?uid=${uid}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetuRolesDropDownHook =(access_token, uid)=>{
	return useQuery(
        ['get-dropdown',access_token, uid],getuRolesDropDownAdmin,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
    
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}

// ## end of chunck
//  start of chunck

export const getPagesDropDownAdmin = async ({ queryKey }) => {
    const access_token = queryKey[1]
    const user_id = queryKey[2]
    return await client({
        method: 'GET',
        url: `/useradmin/pagedropdown?user_id=${user_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const usePagesDropDownHook =(access_token,user_id)=>{
	return useQuery(
        ['get--pages-dropdown',access_token, user_id],getPagesDropDownAdmin,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
    
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}


// ## end of chunck
//  start of chunck


export const deleteUserRoleAdmin = async (data) => {
    return await client({
        method: 'DELETE',
        url: `/useradmin/roles/${data.uid}/${data.role_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Removed Users Role Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
    
} 

export const useDeleteUserRoleHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> deleteUserRoleAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
            queryClient.invalidateQueries('get-single-user')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}
// ## end of chunck
//  start of chunck
export const getSingleEmployee = async ({ queryKey }) => {
    const employee_id = queryKey[1]
    const access_token = queryKey[2]
    return await client({
        method: 'GET',
        url: `/hrm/employee/${employee_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`,
        }
    }).then(function (response) {
            // console.log(response?.data)
            // toast.success('Got roles sucessfully',{
            //     position: 'top-right'
            //   })
            return response
          
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
			return response
        });
       
    } 
    
export const useGetSingleEmployeeHook =(id, access_token)=>{
    
	return useQuery(
        ['get-single-employee',id, access_token], getSingleEmployee,
        {
            useErrorBoundary: true,
            staleTime : 120000,
        },
        {
		onSettled: (data) => {
		return data?.response?.data
			// console.log(data?.response?.status)
		}
        }
          )
}
// ## end of chunck
//  start of chunck

export const deleteUserPageAdmin = async (data) => {
    return await client({
        method: 'DELETE',
        url: `/useradmin/upage/${data.uid}/${data.page_id}`,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data.access_token}`,
        },
    }).then(function (response) {
            // console.log(response?.data?.items)
            toast.success('Removed Users Role Sucessfully',{
                position: 'top-right'
                })
            return response
            
        }).catch((response,error)=> {
            // console.log(response?.response)
            if (response?.response.status == 422){
                toast.error(`${response?.response?.status} - Invalid Data Submited`,{
                    position: 'top-right'
                    })
            } else {
            toast.error(`${response?.response?.status} - ${response?.response?.data.detail}`,{
                position: 'top-right'
                })
            }
            return response
        });
    
} 

export const useDeleteUserPageHook =(data,access_token)=>{
    const queryClient = useQueryClient()
    return useMutation(
        {
        mutationFn : (data)=> deleteUserPageAdmin(data),
        enabled: false,            
        onSuccess : ()=>{
            queryClient.invalidateQueries('get-user-pages')
        },
        onSettled: (data) => {
        return data?.response?.data
            // console.log(data?.response?.status)
        }
        }
            )
}

// ## end of chunck
