import './main.css'
import { useStyle } from './store/theme'
import { BrowserRouter } from 'react-router-dom'
import { MainContent } from './sections/content';
import { SideMenu } from './sections/sidebar';
import { AppMenue } from './sections/dropmenue';
import { QueryClientProvider, QueryClient, QueryCache } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools'
import toast, { Toaster } from 'react-hot-toast'
import ErrorBoundary from './components/errorboundary';

const queryClient= new QueryClient(
    {
        queryCache: new QueryCache({
            onError: (error,response) =>
              toast.error(`${response?.response?.status} - ${response?.response?.data?.detail}`),
          }),
    }
)



function App(){
    const secClass=useStyle((state)=> state.styles.appSecClass)
    return(
        <BrowserRouter>    
            <QueryClientProvider client={queryClient}>
                <section className={secClass}>
                    <ErrorBoundary>
                        <SideMenu />
                    </ErrorBoundary>
                    <ErrorBoundary>
                        <AppMenue />
                    </ErrorBoundary>
                    <MainContent /> 
                    <Toaster />      
                </section>
            {/* <ReactQueryDevtools initialIsOpen={false} position='bottom-right' />   */}
            </QueryClientProvider>
        </BrowserRouter> 
         )
    }


export default App



// npx tailwindcss -i ./src/index.css -m -o ./src/main.css 
// npx tailwindcss -i ./src/main.css -m -o ../static/css/main.min.css --watch
